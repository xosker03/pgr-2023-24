#ifndef FLOATV_H
#define FLOATV_H

#include <math.h>
#include <string>
#include <ostream>

#include "zaklad/zaklad.h"

struct floatV {
    inline floatV() {
        x = 0;
        y = 0;
        z = 0;
    }
    //NEPOUŽÍVAT
    // floatV(float x) {
    //     for(int i = 0; i < FloatV_SIZE; ++i)
    //         data[i] = x;
    // }
    inline floatV(float _x, float _y, float _z = 0.0) {
        x = _x;
        y = _y;
        z = _z;
    }
    inline floatV(const floatV& other) {
        x = other.x;
        y = other.y;
        z = other.z;
    }
    // float* operator*()(int pos) {
    //     return (this->data);
    // }
    // const float* operator*()(int pos) const {
    //     return (this->data);
    // }

    inline float& operator[](int pos) {
        switch(pos){
            case 0: return x;
            case 1: return y;
            case 2: return z;
            default: throw 0;
        }
    }
    inline const float& operator[](int pos) const {
        switch(pos){
            case 0: return x;
            case 1: return y;
            case 2: return z;
            default: throw 0;
        }
    }

    //porovnání
    inline bool operator==(const floatV& right) const {
        return x == right.x && y == right.y && z == right.z;
    }

    //přiřazení
    inline floatV& operator=(const floatV& source) {
        x = source.x;
        y = source.y;
        z = source.z;
        return *this;
    }

    //vektorové sčítání
    inline floatV operator+(const floatV& right) const {
        return floatV(x + right.x, y + right.y, z + right.z);
    }
    inline floatV& operator+=(const floatV& right) {
        x += right.x;
        y += right.y;
        z += right.z;
        return *this;
    }

    //vektorové odčítání
    inline floatV operator-(const floatV& right) const {
        return floatV(x - right.x, y - right.y, z - right.z);
    }
    inline floatV& operator-=(const floatV& right) {
        x -= right.x;
        y -= right.y;
        z -= right.z;
        return *this;
    }

    //vynásobení konstantou
    inline floatV operator*(const float right) const {
        return floatV(x * right, y * right, z * right);
    }
    inline floatV& operator*=(const float right) {
        x *= right;
        y *= right;
        z *= right;
        return *this;
    }
    inline friend floatV operator*(const float left, const floatV& right ) {
        return floatV(left * right.x, left * right.y, left * right.z);
    }

    //vydělení konstantou
    inline floatV operator/(const float right) const {
        return floatV(x / right, y / right, z / right);
    }
    inline floatV& operator/=(const float right) {
        x /= right;
        y /= right;
        z /= right;
        return *this;
    }

    //vektorový součin
    inline floatV operator^(const floatV& b) const {
        floatV ret;
        ret.x = y * b.z - z * b.y;
        ret.y = z * b.x - x * b.z;
        ret.z = x * b.y - y * b.x;
        return ret;
    }

    //skalární součin
    inline float operator&(const floatV& right){
        return x * right.x + y * right.y + z * right.z;
    }

    //délka
    inline float len(){
        return sqrt(x*x + y*y + z*z);
    }
    inline float lenSq(){
        return x*x + y*y + z*z;
    }

    //normalizovaný vektor
    inline floatV norm(){
        return *this / this->len();
    }

    inline bool isnan(){
        return std::isnan(x) || std::isnan(y) || std::isnan(z);
    }

    // floatV kolmice(){
    //     return floatV(this->y, this->x * -1, z);
    // }

    std::string str(){
        return "x:" + std::to_string(x) + ", y:" + std::to_string(y)+ ", z:" + std::to_string(z);
    }

    friend std::ostream & operator<<(std::ostream &str, const floatV &a){
        return str << "< " << a.x << " " << a.y << " " << a.z << " >";
    }

    void print(bool newline = true){
        if(newline)
            //printf("x: %e, y: %e, z: %e\n", x, y, z);
            printf("floatV(%e, %e, %e),\n", x, y, z);
        else
            printf("x: %e, y: %e, z: %e", x, y, z);
    }

    float x;
    float y;
    float z;
};


int floatV_self_test();


#endif
