#version 450 core

uniform vec3 sun = vec3(2,2,2);

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in Vertex {
    vec3 position;
    vec4 color;
    vec4 glPos;
} v[];

out vec4 colorG;

void main() {
    vec3 a = v[0].position;
    vec3 b = v[1].position;
    vec3 c = v[2].position;

    vec3 v1 = b - a;
    vec3 v2 = c - a;

    vec3 normal = normalize(cross(v1, v2));

//     vec3 slunce = normalize(vec3(2, 0.5, 1));
    vec3 slunce = normalize(sun);
    float svitivost = dot(slunce, normal);

    for (int i = 0; i < gl_in.length(); i++) {

        colorG = v[i].color * 0.9 * svitivost + v[i].color * 0.1;
        if(svitivost <= 0)
            colorG = v[i].color * 0.1;
//             colorG[2] += -0.1 * svitivost + 0.05;
        gl_Position = v[i].glPos;
//         gl_Position = vec4(v[i].position, 1);
        EmitVertex();
    }
    EndPrimitive();
}
