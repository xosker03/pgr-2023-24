#version 450 core

#define WORK_GROUP_SIZE 64
// #define WORK_GROUP_CYCLES 1
// 1 až n

uint ggid = 0;

const uint nic_e = 0;
const uint koule_e = (1 << 0);
const uint trojuhelnik_e = (1 << 1);
const uint svetlo_e = (1 << 2);
const uint obal_e = (1 << 3);

struct floatV{
    float x,y,z;
};

struct floatM3{
    float data[3][3];
};

struct color {
    float x,y,z;
};

struct ray {
    vec3 A;
    vec3 p;
};

struct object {
    uint typ;
    floatV bod[3];
    floatV normie;
    floatV stred;
    float r;
    floatV barva;
    float matnost;
//     float pruhlednost;
};

struct camera {
    floatV start;
    floatV p;
    float fov;
    floatM3 rotaceX;
    floatM3 rotaceY;
    floatM3 rotaceZ;
};

struct debug{
    uint u0;
    uint u1;
    uint u2;
    int d0;
    int d1;
    int d2;
    float f0;
    float f1;
    float f2;
    float f3;
};

//////////////////////////////////////////////////////////////////////////////

layout (local_size_x = WORK_GROUP_SIZE, local_size_y = 1, local_size_z = 1) in;
shared color shmem[WORK_GROUP_SIZE];

//////////////////////////////////////////////////////////////////////////////

layout(location = 0) uniform int width = 0;
layout(location = 1) uniform int height = 0;
layout(location = 2) uniform float ratio = 0;
layout(location = 3) uniform int objectCount = 0;
layout(location = 4) uniform uint realSeed = 0;
layout(location = 5) uniform uint WORK_GROUP_CYCLES = 1;
layout(location = 6) uniform uint RAY_BOUNCES = 3;
layout(location = 7) uniform uint AVERAGE_RENDER = 0;
layout(location = 8) uniform int OBJ_OFFSET = 0;


// layout (binding = 0) buffer DataBlock0 {
//     object objekty[];
// };

layout (binding = 1) buffer DataBlock1 {
    color barvy[];
};

layout (binding = 2) buffer DataBlock2 {
    debug debuginfo[];
};

layout (binding = 3) buffer DataBlock3 {
    camera cam;
};

layout (binding = 4) buffer DataBlock4 {uint obj_typ[];};
layout (binding = 5) buffer DataBlock5 {floatV obj_bod0[];};
layout (binding = 6) buffer DataBlock6 {floatV obj_bod1[];};
layout (binding = 7) buffer DataBlock7 {floatV obj_bod2[];};
layout (binding = 8) buffer DataBlock8 {floatV obj_normie[];};
layout (binding = 9) buffer DataBlock9 {floatV obj_stred[];};
layout (binding = 10) buffer DataBlock10 {float obj_r[];};
layout (binding = 11) buffer DataBlock11 {floatV obj_barva[];};
layout (binding = 12) buffer DataBlock12 {float obj_matnost[];};

layout (binding = 0, rgba8) writeonly uniform image2D outputImage;

//////////////////////////////////////////////////////////////////////////////


//FLOATV
vec3 vectorize(floatV x){
    return vec3(x.x, x.y, x.z);
}
floatV floatize(vec3 x){
    floatV r;
    r.x = x.x;
    r.y = x.y;
    r.z = x.z;
    return r;
}

mat3 matricize(floatM3 x){
    return mat3(
        x.data[0][0], x.data[0][1], x.data[0][2],
        x.data[1][0], x.data[1][1], x.data[1][2],
        x.data[2][0], x.data[2][1], x.data[2][2]
    );
}

//COLOR
color colorMix(color l, color r){
    color ret;
    ret.x = l.x * r.x;
    ret.y = l.y * r.y;
    ret.z = l.z * r.z;
    return ret;
}
color colorScale(color l, float r){
    color ret;
    ret.x = l.x * r;
    ret.y = l.y * r;
    ret.z = l.z * r;
    return ret;
}
color colorAdd(color l, color r){
    color ret;
    ret.x = l.x + r.x;
    ret.y = l.y + r.y;
    ret.z = l.z + r.z;
    return ret;
}
color colorWrite(color l){
//     float r = l.x; float g = l.y; float b = l.z;
    color r = l;
    float scale = 1.0 / (WORK_GROUP_SIZE * WORK_GROUP_CYCLES * gl_NumWorkGroups.z);
    r.x *= scale;
    r.y *= scale;
    r.z *= scale;
    r.x = clamp(sqrt(r.x), 0.0, 1.0);
    r.y = clamp(sqrt(r.y), 0.0, 1.0);
    r.z = clamp(sqrt(r.z), 0.0, 1.0);
    return r;
}

color newColor(float _r, float _g, float _b){
    color R;
    R.x = _r; R.y = _g; R.z = _b;
    return R;
}
color colorize(floatV c){
    color R;
    R.x = c.x; R.y = c.y; R.z = c.z;
    return R;
}

//OBJECT
float prunik(uint oid, ray paprsek){
    if((obj_typ[oid] & koule_e) > 0 || (obj_typ[oid] & obal_e) > 0){
        vec3 a = paprsek.A;
        vec3 p = paprsek.p;
        vec3 c = vectorize(obj_stred[oid]);
        float aa = dot(p,p);
        float bb = 2 * dot(p, (a - c));
        float cc = dot((a - c), (a - c)) - obj_r[oid] * obj_r[oid];
        float D = bb * bb - 4*aa*cc;
        if(D >= 0){
            float sD = sqrt(D);
//             float x1 = (-bb + sD) / (2 * aa);
            float x2 = (-bb - sD) / (2 * aa);
//             return min(x1, x2);
            return x2;
        }
    }
    if((obj_typ[oid] & trojuhelnik_e) > 0){
        const float EPSILON = 0.000001;

        const vec3 v0 = vectorize(obj_bod0[oid]);
        const vec3 v1 = vectorize(obj_bod1[oid]);
        const vec3 v2 = vectorize(obj_bod2[oid]);

        vec3 edge1, edge2, h, s, q;
        float a, f, u, v;

        edge1 = v1 - v0;
        edge2 = v2 - v0;

        h = cross(paprsek.p, edge2);
        a = dot(edge1, h);

        if (a > -EPSILON && a < EPSILON)
            return -1;

        f = 1.0f / a;
        s = paprsek.A - v0;
        u = f * dot(s, h);

        if (u < 0.0 || u > 1.0)
            return -1;

        q = cross(s, edge1);
        v = f * dot(paprsek.p, q);

        if (v < 0.0 || u + v > 1.0)
            return -1;

        float t = f * dot(edge2, q);

        if (t > EPSILON)
            return t;
    }
    return -1;
}
vec3 normala(uint oid, vec3 X){
    if((obj_typ[oid] & koule_e) > 0){
        vec3 s = vectorize(obj_stred[oid]);
        vec3 n = X - s;
        return normalize(n);
    }
    if((obj_typ[oid] & trojuhelnik_e) > 0){
//         return -vectorize(obj_normie[oid]);
        vec3 A1 = vectorize(obj_bod0[oid]);
        vec3 A2 = vectorize(obj_bod1[oid]);
        vec3 A3 = vectorize(obj_bod2[oid]);
        return -normalize(cross(A2 - A1, A3 - A1));
    }
    return vec3(0,0,0);
}

//RAY
ray newRay(vec3 start, vec3 end){
    ray r;
    r.A = start;
    r.p = normalize(end - start);
    return r;
}
vec3 getPoint(ray R, float t){
    return R.A + t * R.p;
}


uint fastRnd32(uint fast_seed){
    fast_seed ^= (fast_seed << 13);
    fast_seed ^= (fast_seed >> 17);
    fast_seed ^= (fast_seed << 5);
    return fast_seed;
}
const uint UINT32_MAX = 0xffffffff;
float fRf(uint fast_seed){
    return fast_seed / (UINT32_MAX + 1.0);
}
vec3 randomDirection(uint state){
    uint a1 = fastRnd32(state);
    uint a2 = fastRnd32(state + a1);
    uint a3 = fastRnd32(state + a1 + a2);
    vec3 d = vec3(fRf(a1)*2.0-1.0, fRf(a2)*2.0-1.0, fRf(a3)*2.0-1.0);
    return normalize(d);
}

//////////////////////////////////////////////////////////////////////////////

color rayCast(ray R, uint budget, int skipoid, uint seed){
    color zakladovka = colorize(floatize(vec3(1,1,1)));
    int hits = 0;
    int light = 0;

    while(budget > 0){
        int nejblizsi_oid = -1;
        float vzdalenost = 1. / 0.;

        for(int i = 0; i < objectCount; ++i){
            if(i == skipoid){
                continue;
            }
            float pr = prunik(i, R);
            if(!isnan(pr) && !isinf(pr)){
                if(pr > 0 && pr < vzdalenost){
                    if((obj_typ[i] & obal_e) > 0){
                        int no = -1;
                        float vz = 1. / 0.;
                        int start = int(obj_normie[i].x) + OBJ_OFFSET;
                        int stop = start + int(obj_normie[i].y);
                        for(int i = start; i < stop; ++i){
                            if(i == skipoid){
                                continue;
                            }
                            float pr = prunik(i, R);
                            if(!isnan(pr) && !isinf(pr)){
                                if(pr > 0 && pr < vz){
                                    no = i;
                                    vz = pr;
                                }
                            }
                        }
                        if(no >= 0){
                            nejblizsi_oid = no;
                            vzdalenost = vz;
                        }
                    }
                    else
                    {
                        nejblizsi_oid = i;
                        vzdalenost = pr;
                    }
                }
            }
        }
        if(nejblizsi_oid >= 0){
            vec3 X = getPoint(R, vzdalenost);
            vec3 norm = normala(nejblizsi_oid, X);
            if((obj_typ[nejblizsi_oid] & svetlo_e) > 0 || RAY_BOUNCES == 1){
                float skalar = dot(norm, R.p);
				zakladovka = colorScale(zakladovka, 3.0);
                zakladovka = colorScale(colorMix(zakladovka, colorize(obj_barva[nejblizsi_oid])), -skalar);
                light++;
                break;
            } else {
                if(obj_matnost[nejblizsi_oid] == 0.0){
                    R.A = X;
                    R.p = reflect(R.p, norm);
                } else {
                    seed += fastRnd32(seed) + ggid * (gl_LocalInvocationID.x + 1) * (gl_WorkGroupID.y + 1) * (gl_WorkGroupID.x + 1) + realSeed + 1;
                    float matrnd = fRf(fastRnd32(seed));
                    if(matrnd <= obj_matnost[nejblizsi_oid]){
                        R = newRay(vec3(0,0,0), randomDirection(seed));
                        R.A = X;
                        if(dot(R.p, norm) < 0.0){
                            R.p *= -1.0;
                        }
                        zakladovka = colorScale(zakladovka, dot(norm, R.p));
                    } else {
                        R.A = X;
                        R.p = reflect(R.p, norm);
                    }
                }
                zakladovka = colorMix(zakladovka, colorize(obj_barva[nejblizsi_oid]));
                skipoid = nejblizsi_oid;
                hits++;
            }
        }
        --budget;
    }
    if(light > 0){
        return zakladovka;
    } else {
        if(hits > 0){
            return newColor(0.0, 0.0, 0.0);
//             return colorMix(zakladovka, newColor(0.1, 0.1, 0.1));
        }
    }
    return newColor(0.0, 0.0, 0.0);
//     return newColor(0.1, 0.1, 0.1);
}


void main() {
    uint gid = gl_WorkGroupID.x + gl_NumWorkGroups.x * gl_WorkGroupID.y;
    uint idx = gl_WorkGroupID.x;
    uint idy = gl_WorkGroupID.y;

    ggid = gid;

    if(idx <= width && idy <= height){
        float x = idx * 2.0 / (width) - 1.0;
        float y = idy * 2.0 / (height) - 1.0;

        vec3 smer = vec3(x * cam.fov, y * cam.fov * ratio, 1);
        smer = smer * matricize(cam.rotaceX) * matricize(cam.rotaceY) * matricize(cam.rotaceZ);
        smer = normalize(smer);

        ray pap = newRay(vectorize(cam.start), vectorize(cam.start) + smer);

        shmem[gl_LocalInvocationID.x] = rayCast(pap, RAY_BOUNCES, -1, 0);
        for(uint i = 1; i < WORK_GROUP_CYCLES; ++i){
            shmem[gl_LocalInvocationID.x] = colorAdd(shmem[gl_LocalInvocationID.x], rayCast(pap, RAY_BOUNCES, -1, fastRnd32(i)));
        }

    }

    memoryBarrier();
    for(uint i = WORK_GROUP_SIZE / 2; i > 0; i /= 2){
        if(gl_LocalInvocationID.x < i){
            shmem[gl_LocalInvocationID.x] = colorAdd(shmem[gl_LocalInvocationID.x], shmem[gl_LocalInvocationID.x + i]);
        }
        memoryBarrier();
    }
    if(gl_LocalInvocationID.x == 0){
        color ret = colorWrite(shmem[0]);
        if(AVERAGE_RENDER > 0){
            barvy[gid] = colorScale(colorAdd(colorScale(barvy[gid], AVERAGE_RENDER), ret), 1.0/(AVERAGE_RENDER + 1));
            ret = barvy[gid];
        }
        imageStore(outputImage, ivec2(gl_WorkGroupID.xy), vec4(ret.x,ret.y,ret.z,1));
    }
}



