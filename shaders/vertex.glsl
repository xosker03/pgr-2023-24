#version 450 core

uniform mat4 view = mat4(1);
uniform mat4 proj = mat4(1);

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 color;

out vec3 colorV;
out vec2 texCoord;
// out Vertex {
//     vec3 position;
//     vec4 color;
//     vec4 glPos;
// } v;


void main(){
    colorV = color;
//     v.position = aPos;
//     v.color = color;
//     gl_Position = proj * view * vec4(aPos.x, aPos.y, aPos.z, 1.0);
    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
    texCoord = vec2(color.x, color.y);
//     v.glPos = gl_Position;
}
