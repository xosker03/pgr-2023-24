#version 450 core

// in vec3 colorV;
//
// out vec4 FragColor;
//
// void main(){
//     FragColor = vec4(colorV, 1.0);
//     //FragColor = colorG;
// }

// layout (binding = 0, rgba8) readonly uniform image2D inputImage;
uniform sampler2D inputImage;

in vec2 texCoord;

out vec4 FragColor;

void main() {
//     vec4 color = imageLoad(inputImage, ivec2(gl_FragCoord.xy));
    vec4 color = texture(inputImage, texCoord);
    FragColor = color;
}
