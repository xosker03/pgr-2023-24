#ifndef SIMULATION_H
#define SIMULATION_H

#include <vector>
#include <tuple>

#include "timer.h"
#include "Particle.h"
#include "Object.h"


class Simulation
{
public:
    Simulation(bool sync = true, int typ_kulicek = 8);
    ~Simulation();
    void init();
    void run();
    bool step();

    void handleCollisions(Particle & ball1, Particle & ball2);
    void handlePendulum(Particle & ball1, Particle & ball2);
    void handleBorders(Particle & a);

    int64_t tocid(Particle & a);
    int64_t tocid(int64_t x, int64_t y, int64_t z);
    std::tuple<int64_t, int64_t, int64_t> fromcid(Particle & a);
    std::vector<uint64_t> adjecentcid(Particle & a);

    static float gravity_force(float weight);
    static float acceleration(float force, float weight);

    float m_gravity_enabled = 0.0f;
    volatile bool m_sim_pause = true;
    floatV m_ext_acceleration = floatV(0,0,0);
    double m_energy = 0;

    float m_dt = 1.0e-3f; // s
    double m_time = 0; // s
    double m_end_time = 100.0; // s
    //float m_end_time = 10.16; // s

    floatV _wire = floatV(0.5f, 0.5f,0);
    float _wireRadius = 0.15f;

    bool sync_gl = false;

    std::vector<Particle *> m_data;
    std::vector<Object *> m_objects;

    Timer timer;
    float step_time_ms;

    struct settings {
        floatV gravity_accell;
        float dumping_perpen;
        float dumping;
        float restitution;
        floatV wall_l;
        floatV wall_r;
        floatV center;
    } m_sett = {
        .gravity_accell = floatV(0, -9.81,0),
        .dumping_perpen = 0.999f,
        .dumping = (0.95f + (1 - 0.999f)),
        .restitution = 0.99f,
        .wall_l = floatV(0.0, 0.0, 0.0),
        .wall_r = floatV(50, 27, 50)
    };

    int64_t cells;
    int64_t cells_dim[3];
    std::vector<int64_t> cells_starts;
    float max_diameter = 0;

    bool use_simple_collisions = false;
};

#endif // SIMULATION_H
