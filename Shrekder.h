#ifndef Shrekder_H
#define Shrekder_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class Shrekder{
public:
    Shrekder(std::string name = "Program");
	~Shrekder();

    void compile(GLenum type, std::string name = "shader", std::string file = "");
    void compile_vertex();
    void compile_fragment();
    void link();
    void use();

    unsigned int m_mainProgram;

private:
    std::string read_file(const char* file_path);

    std::string m_programName;
    std::vector<unsigned int> m_shader;

    bool linked = false;
};


#endif

