#ifndef __ZAKLAD_H__
#define __ZAKLAD_H__

/* Použitelná Marka
 *
 * ZAKLAD_NO_OUTPUT
 * ZAKLAD_TO_STDERR
 * ZAKLAD_NO_COLORS
 *
 * ZAKLAD_NO_MUTEX
 * ZAKLAD_NO_FLUSH
 *
 */


#include <string>
#include <stdint.h>

#ifndef ZAKLAD_NO_OUTPUT
	#ifndef ZAKLAD_TO_STDERR
		#define printd(...) fprintf(stdout, __VA_ARGS__)
	#else
		#define printd(...) fprintf(stderr, __VA_ARGS__)
	#endif
#else
	#define printd(...) voidfun()
#endif


struct bcolors{
	const char *BLUE;
	const char *GREEN;
	const char *RED;
	const char *VIOLET;
	const char *YELLOW;
	const char *CYAN;
	const char *DBLUE;
	const char *DGREEN;
	const char *DRED;
	const char *DVIOLET;
	const char *DYELLOW;
	const char *DCYAN;
	const char *WHITE;
	const char *DGRAY;
	const char *GRAY;
	const char *BLACK;
	const char *BOLD;
	const char *UNDERLINE;
	const char *CRITIC;
	const char *ENDC;
	const char *CURSORH;
	const char *CURSORS;
	const char *chyba;
};

extern struct bcolors bc;


#define printblue(...)	printc(bc.BLUE, __VA_ARGS__)
#define printgreen(...)	printc(bc.GREEN, __VA_ARGS__)
#define printred(...)	printc(bc.RED, __VA_ARGS__)
#define printviolet(...)	printc(bc.VIOLET, __VA_ARGS__)
#define printyellow(...)	printc(bc.YELLOW, __VA_ARGS__)
#define printcyan(...)	printc(bc.CYAN, __VA_ARGS__)

#define printdblue(...)	printc(bc.DBLUE, __VA_ARGS__)
#define printdgreen(...)	printc(bc.DGREEN, __VA_ARGS__)
#define printdred(...)	printc(bc.DRED, __VA_ARGS__)
#define printdviolet(...)	printc(bc.DVIOLET, __VA_ARGS__)
#define printdyellow(...)	printc(bc.DYELLOW, __VA_ARGS__)
#define printdcyan(...)	printc(bc.DCYAN, __VA_ARGS__)

bool printif(bool p);
bool printif(bool p, const char * data, ...);

void printc(const char * color);
void printc(const char * color, const char * data, ...);


void printendc();
int voidfun();



namespace Zaklad{

    extern uint32_t fast_seed;

    uint8_t rnd8();
    uint32_t rnd32();
    float rndf();

    void fastSrand();
    uint32_t fastRnd32();
    float fastRndf();


}
#endif
