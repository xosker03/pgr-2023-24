#include "shell.h"
#include "zaklad.h"

std::string shell_pipe(const std::string cmd, int& out_exitStatus)
{
    out_exitStatus = 0;
    auto pPipe = ::popen(cmd.c_str(), "r");
    if(pPipe == nullptr){
        throw std::string("Cannot open pipe");
    }

    std::array<char, 256> buffer;

    std::string result;

    while(not std::feof(pPipe)){
        auto bytes = std::fread(buffer.data(), 1, buffer.size(), pPipe);
        result.append(buffer.data(), bytes);
    }

    auto rc = ::pclose(pPipe);

    if(WIFEXITED(rc)){
        out_exitStatus = WEXITSTATUS(rc);
    }

    return result;
}


int shell_call(const std::string cmd){
    std::cout << bc.BLUE << cmd << bc.ENDC << std::endl;
    auto pPipe = ::popen((cmd).c_str(), "r");
    if(pPipe == nullptr){
        throw std::string("Cannot open pipe");
    }
    char tmp[1024];
    while(not std::feof(pPipe)){
        std::fread(tmp, 1, 1024, pPipe);
    }
    int rc = ::pclose(pPipe);
    return rc;
}

void shell_throw(const std::string cmd){
    std::thread first (shell_call, cmd + " 2>&1");
    first.detach();
}


