/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <sys/time.h>


#define timerStart() gettimeofday(&__start, NULL)
#define timerStop() gettimeofday(&__stop, NULL)
#define timer_us ((1000000 * __stop.tv_sec + __stop.tv_usec) - (1000000 * __start.tv_sec + __start.tv_usec))
#define timer_ms (timer_us / 1000.0)
#define timer_s (timer_ms / 1000.0)

extern struct timeval __stop, __start;




class Timer
{
public:
    Timer(){}
    ~Timer(){}

    inline void start(){
        gettimeofday(&m_start, NULL);
    }
    inline void stop(){
        gettimeofday(&m_stop, NULL);
    }

    inline unsigned long us(){
        return ((1000000 * m_stop.tv_sec + m_stop.tv_usec) - (1000000 * m_start.tv_sec + m_start.tv_usec));
    }
    inline double ms(){
        return us()/1000.0;
    }
    inline double s(){
        return ms() / 1000.0;
    }

    struct timeval m_start;
    struct timeval m_stop;
};



#endif
