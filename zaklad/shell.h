#ifndef __SHELL_H__
#define __SHELL_H__

#include <string>
#include <array>
#include <iostream>
#include <thread>



std::string shell_pipe(const std::string cmd, int& out_exitStatus);
int shell_call(const std::string cmd);
void shell_throw(const std::string cmd);


#endif
