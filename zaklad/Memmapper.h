#ifndef Memmapper_H
#define Memmapper_H

#include <stdint.h>
#include <iostream>


class Memmapper{
public:
    Memmapper(const char * filename, bool pagelock = false);
    Memmapper(std::string filename, bool pagelock = false);
	~Memmapper();

    void create(uint64_t size);
    void read();
    void readwrite();
    void mclose();

    uint64_t size();
    char * data();

private:
    void mopen(int prot);
    void map(int prot);

    bool m_opened = false;
    bool m_pagelock = false;
    int m_fd = 0;
    uint64_t m_size = 0;
    std::string m_filename;
    char * m_data;
};


#endif

