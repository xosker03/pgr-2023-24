#include "Dengine.h"

#include<GL/glut.h>

#include "../zaklad/timer.h"
#include "../zaklad/zaklad.h"

#include "../floatV.h"

#include "Raytracer.h"

static Timer __timer;
static Timer __timer2;

namespace Dengine {
    bool READY = false;

    bool stop = false;
    int X_size = 1280;
    int Y_size = 720;

    floatV cam_pos(4,5,6);
    floatV center(4, 0, 1.2);

    bool rotate_camera = false;
    floatV mouse_pos;
    floatV cam_angle(0.1, 0.1, 0.2);
    float fov = 45.0;

    bool move_camera_y = false;
    bool move_camera_xz = false;

    Raytracer rayEngine;
    std::vector<struct Point> colors;
}


void Dengine::Glengine(int argc, char** argv, int width, int height)
{
    __timer.start();

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glutInitWindowSize(X_size, Y_size);
    //glutInitWindowPosition(900, 20);
    glutCreateWindow("GL Engine");

    // glEnable(GL_POINT_SMOOTH);
    // glPointSize(4.0);

    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutIdleFunc(update);


    glutKeyboardFunc(keyPress);
    glutMouseFunc(mousePress);
    glutMotionFunc(mouseMotion);

    //INIT
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glColor3f(1.0, 1.0, 1.0);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    rayEngine.lateInit(false, width, height);
    colors.resize(width * height);

    READY = true;
}

void Dengine::reshape(GLint w, GLint h) {
    //printblue("Reshape %d %d\n", w ,h);
    glViewport(0, 0, w, h);
    X_size = w;
    Y_size = h;
    setCamera();
}

void Dengine::mainLoop(){
    glutMainLoop();
}

void Dengine::display()
{
    __timer2.start();
    // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBegin(GL_LINES);
        glColor3f(1, 0, 0);
            glVertex3f(0, 0, 0); glVertex3f(1, 0, 0);
        glColor3f(0, 1, 0);
            glVertex3f(0, 0, 0); glVertex3f(0, 1, 0);
        glColor3f(0, 0, 1);
            glVertex3f(0, 0, 0); glVertex3f(0, 0, 1);
    glEnd();

    //Kroužek
    // glColor3f(1.0, 0.0, 0.0);
    // glTranslated( m_sim->_wire.x, m_sim->_wire.y, 0);
    // glutSolidSphere(m_sim->_wireRadius *2, 60, 60);
    // glColor3f(0.0, 0.0, 0.0);
    // glTranslated( m_sim->_wire.x, m_sim->_wire.y, 0);
    // glutSolidSphere(m_sim->_wireRadius *2 * 0.99, 60, 60);

    // glColor3f(a->R, a->G, a->B);
    // glPushMatrix();
    // glTranslated(a->m_pos.x, a->m_pos.y, a->m_pos.z);
    // int quality = a->m_radius * 1000.0;
    // quality = (quality < 8)?8:quality;
    // //glutWireSphere(a->m_radius, quality, quality);
    // //glutSolidSphere(a->m_radius, quality, quality);
    // glutSolidSphere(a->m_radius, 6, 6);
    // glPopMatrix();


    glFlush();
    glutSwapBuffers();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    __timer.stop();
    __timer2.stop();
    printc(bc.BLUE, "\rFPS: %.1f/%.1f ", 1000.0 / __timer2.ms(), 1000.0 / __timer.ms());
    __timer.start();
}

void Dengine::redisplay(){
    glutPostRedisplay();
}

// void Glengine::timer(int v)
// {
//     //glLoadIdentity();
//     glutPostRedisplay();
//     if(!stop)
//         glutTimerFunc(1000/60.0, timer, v);
//     // else
//     //     glutDestroyWindow(1);
// }

void Dengine::drawLine(floatV from, floatV to, floatV color){
    if(READY){
        glBegin(GL_LINES);
        glColor3f(color.x, color.y, color.z);
        glVertex3f(from.x, from.y, from.z * -1.0); glVertex3f(to.x, to.y, to.z * -1.0);
        glEnd();
    }
}
void Dengine::drawSphere(floatV from, float radius, floatV color){
    if(READY){
        glColor3f(color.x, color.y, color.z);
        glPushMatrix();
        glTranslated(from.x, from.y, from.z * -1.0);
        glutSolidSphere(radius, 30, 30);
        glPopMatrix();
    }
}

void Dengine::update()
{
    // for(int i = 0; i < sim_steps_not_render; ++i)
    //     m_sim->step();
    rayEngine.getFrame(colors);
    redisplay();
}

void Dengine::setCamera(){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fov, GLfloat(X_size)/GLfloat(Y_size), 1, 1000);
    glMatrixMode(GL_MODELVIEW);
    computeCameraAngle(0,0);
    rotateCamera();
}

void Dengine::rotateCamera(){
    glLoadIdentity();
    gluLookAt(cam_pos.x, cam_pos.y, cam_pos.z, center.x, center.y, center.z, 0, 1, 0);
}

void Dengine::computeCameraAngle(int x, int y){
    float dx = (mouse_pos.x - x);
    float dy = -1.0 * (mouse_pos.y - y);
    cam_angle += floatV(dx, dy, 0) * 0.01;
    float nx = sin(cam_angle.x);
    float ny = sin(cam_angle.y);
    float nz = cos(cam_angle.x) * cos(cam_angle.y);
    cam_pos = floatV(nx, ny, nz) * 15.0;// + floatV(0,0,cam_pos.z);
    mouse_pos = floatV(x,y, 0);
    //printf("cam_pos %s | cam_angle %s\n", cam_pos.str().c_str(), cam_angle.str().c_str());
    rotateCamera();
}


void Dengine::mousePress(int button, int state, int x, int y)
{
    float px = x * 1.0 / X_size;
    float py = (Y_size - y) * 1.0 / Y_size;
    floatV mouse(px, py, 0);
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        // for(auto & a : m_sim->m_data){
        //     if((a->m_pos - mouse).len() <= a->m_radius){
        //         a->m_euler_enabled = false;
        //         particle_drag = a;
        //         break;
        //     }
        // }
        mouse_pos = floatV(x,y, 0);
        move_camera_xz = true;
    }
    if(button == GLUT_LEFT_BUTTON && state == GLUT_UP){
        // if(particle_drag){
        //     particle_drag->m_euler_enabled = true;
        //     particle_drag = NULL;
        // }
        move_camera_xz = false;
        mouse_pos = floatV(0,0, 0);
    }
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN){
        mouse_pos = floatV(x,y, 0);
        rotate_camera = true;
    }
    if(button == GLUT_RIGHT_BUTTON && state == GLUT_UP){
        rotate_camera = false;
        mouse_pos = floatV(0,0, 0);
    }
    if(button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN){
        mouse_pos = floatV(x,y, 0);
        move_camera_y = true;
    }
    if(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP){
        move_camera_y = false;
        mouse_pos = floatV(0,0, 0);
    }
    if(button == 3){
        fov -= 1.0;
        mouse_pos = floatV();
        setCamera();
    }
    if(button == 4){
        fov += 1.0;
        mouse_pos = floatV();
        setCamera();
    }
}

void Dengine::mouseMotion(int x, int y)
{
    float px = x * 1.0 / X_size;
    float py = (Y_size - y) * 1.0 / Y_size;
    floatV mouse(px, py, 0);
    if(rotate_camera){
        computeCameraAngle(x, y);
    }
    if(move_camera_y){
        floatV dm = mouse_pos - floatV(x,y, 0);
        if(dm.y > 0){
            center.y -= 0.001f;
            rotateCamera();
            mouse_pos = floatV(x,y, 0);
        }
        if(dm.y < 0){
            center.y += 0.001f;
            rotateCamera();
            mouse_pos = floatV(x,y, 0);
        }
    }
    if(move_camera_xz){
        floatV dm = mouse_pos - floatV(x,y, 0);
        if(dm.y > 0){
            center.z += 0.001f;
        }
        if(dm.y < 0){
            center.z -= 0.001f;
        }
        if(dm.x > 0){
            center.x += 0.001f;
        }
        if(dm.x < 0){
            center.x -= 0.001f;
        }
        rotateCamera();
        mouse_pos = floatV(x,y, 0);
    }
}


void Dengine::keyPress(unsigned char key, int x, int y){
    if(key == 'Q'){
        printyellow("[%c] EXIT\n", key);
        auto windowID = glutGetWindow();
        glutDestroyWindow(windowID);
        exit(0);
    }

    // if(key == ' '){
    //     m_sim->m_sim_pause = !m_sim->m_sim_pause;
    //     printif(m_sim->m_sim_pause,"[%c] Pause\n", key);
    //     return;
    // }
    // if(key == 'g'){
    //     if(m_sim->m_gravity_enabled)
    //         m_sim->m_gravity_enabled = 0.0f;
    //     else
    //         m_sim->m_gravity_enabled = 1.0f;
    //     printif(m_sim->m_gravity_enabled,"[%c] Gravity\n", key);
    //     return;
    // }

    // if(key == 'w'){
    //     m_sim->m_ext_acceleration += floatV(0, 2.0e0);
    //     printgreen("[%c] ext accel %s\n", key, m_sim->m_ext_acceleration.str().c_str());
    // }
    // if(key == 'a'){
    //     m_sim->m_ext_acceleration += floatV(-2.0e0, 0);
    //     printgreen("[%c] ext accel %s\n", key, m_sim->m_ext_acceleration.str().c_str());
    // }
    // if(key == 's'){
    //     m_sim->m_ext_acceleration += floatV(0, -2.0e0);
    //     printgreen("[%c] ext accel %s\n", key, m_sim->m_ext_acceleration.str().c_str());
    // }
    // if(key == 'd'){
    //     m_sim->m_ext_acceleration += floatV(2.0e0, 0);
    //     printgreen("[%c] ext accel %s\n", key, m_sim->m_ext_acceleration.str().c_str());
    // }
    // if(key == 'q'){
    //     m_sim->m_ext_acceleration += floatV(0, 0, +2.0e0);
    //     printgreen("[%c] ext accel %s\n", key, m_sim->m_ext_acceleration.str().c_str());
    // }
    // if(key == 'e'){
    //     m_sim->m_ext_acceleration += floatV(0, 0, -2.0e0);
    //     printgreen("[%c] ext accel %s\n", key, m_sim->m_ext_acceleration.str().c_str());
    // }
    // if(key == 'r'){
    //     m_sim->m_ext_acceleration = floatV(0, 0);
    //     printgreen("[%c] ext accel %s\n", key, m_sim->m_ext_acceleration.str().c_str());
    // }
    //
    // if(key == 'x'){
    //     m_sim->m_dt /= 10.0;
    //     printif(m_sim->m_dt > 9.0e-5l, "[%c] dt %e\n", key, m_sim->m_dt * 1.0000001);
    // }
    // if(key == 'y'){
    //     m_sim->m_dt *= 10.0;
    //     printif(m_sim->m_dt > 9.0e-5l, "[%c] dt %e\n", key, m_sim->m_dt * 1.0000001);
    // }

    // if(key == 'c'){
    //     --sim_steps_not_render;
    //     printgreen("[%c] not render steps %d\n", key, sim_steps_not_render);
    // }
    // if(key == 'v'){
    //     ++sim_steps_not_render;
    //     printgreen("[%c] not render steps %d\n", key, sim_steps_not_render);
    // }
    // if(key == 'C'){
    //     sim_steps_not_render /= 10;
    //     printgreen("[%c] not render steps %d\n", key, sim_steps_not_render);
    // }
    // if(key == 'V'){
    //     sim_steps_not_render *= 10;
    //     printgreen("[%c] not render steps %d\n", key, sim_steps_not_render);
    // }

    // if(key == 'b'){
    //     draw_velocities = !draw_velocities;
    // }
    // if(key == 'B'){
    //     draw_balls = !draw_balls;
    // }

    // if(key == 'R'){
    //     printyellow("[%c] RESTART\n", key);
    //     bool sync = m_sim->sync_gl;
    //     bool pause = m_sim->m_sim_pause;
    //     m_sim->~Simulation();
    //     new (m_sim) Simulation(sync);
    //     m_sim->m_sim_pause = pause;
    // }
}





