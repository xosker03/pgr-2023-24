#ifndef Raytracer_H
#define Raytracer_H

#include <iostream>
#include <vector>

#include "Raytracer/RayImpl.h"
#include "Raytracer/RayGPU.h"
#include "zaklad/timer.h"

struct Vertex{
    float x, y, z;
    // float r, g, b, a;
};

struct Point{
    float r, g, b;
};


// #define IMPL RayTr
#define IMPL RayGPU

class Raytracer{
public:
    Raytracer(bool async, int width, int height);
    Raytracer();
	~Raytracer();

    void lateInit(bool async, int width, int height);
    void getFrame(std::vector<struct Point> & target);

    void genFrame();
    void run();

    Timer tim;

    int m_width;
    int m_height;
    float m_fov;
    IMPL::camera cam;

    Settings m_sett;

private:
    bool m_async;

    std::vector<struct Point> m_points;
};


#endif

