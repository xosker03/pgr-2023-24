#ifndef GLENGINE_H
#define GLENGINE_H

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "imgui/imgui.h"
#include "imgui/backends/imgui_impl_glfw.h"
#include "imgui/backends/imgui_impl_opengl3.h"

#include "Bufferino.h"
#include "Shrekder.h"

#include "Raytracer.h"


#include "zaklad/timer.h"


class GLEngine{
public:
    GLEngine(int ray_width = 640, int ray_height = 360, int gl_width = 1280, int gl_height = 720);
	~GLEngine();

    void mainLoop();
    void raytracerInit(int width, int height);

    void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

    GLuint imageTexture;

    bool m_simulate = false;
    bool m_run_raytracer = true;
    bool m_stop_on_average = false;
    int num_objs = 0;

private:
    void newFrame();
    void render();

    void guiNewFrame();
    void guiUpdate();
    void guiRender();

    GLFWwindow* window;
    ImGuiIO * io;
    // bool show_demo_window = false;
    bool show_another_window = true;
    // ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    bool render_points = false;

    std::vector<struct Vertex> vertices;
    std::vector<struct Point> colors;
    std::vector<unsigned int> indices;
    Buffik<struct Vertex, struct Point> buff;
    Shrekder program;

    Raytracer rayEngine;

    Timer timRedner;

};

extern class GLEngine * ENGINE;

#endif

