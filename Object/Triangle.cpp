#include "Triangle.h"

#include <math.h>

#include "Bead.h"

// static float obsah(floatV A, floatV B, floatV C){
//     floatV b = C - A;
//     floatV c = B - A;
//     float alfa = acos((b & c) / (b.len() * c.len()));
//     float vc = b.len() * sin(alfa);
//     return c.len() * vc * 0.5;
// }


Triangle::Triangle(floatV pos, floatV speed, float radius) : Object()
{
    m_particles.push_back( new Particle (pos + floatV(-0.03, -0.03, 0.0), speed, 0.015, Particle::default_e) );
    m_particles.push_back( new Particle (pos + floatV(0.0, 0.03, 0.0), speed, 0.015, Particle::default_e) );
    m_particles.push_back( new Particle (pos + floatV(0.03, -0.03, 0.0), speed, 0.015, Particle::default_e) );

    m_subobjects.push_back(new Bead(m_particles[0], m_particles[1]));
    m_subobjects.push_back(new Bead(m_particles[1], m_particles[2]));
    m_subobjects.push_back(new Bead(m_particles[2], m_particles[0]));

    // m_obsah = obsah(A->m_pos, B->m_pos, C->m_pos);
    //printf("%f %f\n", m_obsah, 0.05 * 0.05 / 2.0);

    for(auto & a : m_particles){
        a->R = 1;
        a->G = 1;
        a->B = 0;
    }
}

Triangle::Triangle(Particle* p0, Particle* p1, Particle* p2, float alpha)
{
    c_alpha = alpha;
    pseudo_object = true;
    m_sdt = 1;
    m_particles.push_back(p0);
    m_particles.push_back(p1);
    m_particles.push_back(p2);
    m_subobjects.push_back(new Bead(m_particles[0], m_particles[1], c_alpha));
    m_subobjects.push_back(new Bead(m_particles[1], m_particles[2], c_alpha));
    m_subobjects.push_back(new Bead(m_particles[2], m_particles[0], c_alpha));
}


Triangle::~Triangle()
{

}


// float Triangle::Ct(floatV A, floatV B, floatV C){
//     return obsah(A, B, C) - m_obsah;
// }
//
//
// floatV Triangle::GCt(floatV A, floatV B, floatV C){
//     return (B-A) / (B-A).len() + (C-A) / (C-A).len();
// }



void Triangle::solve_constraints(){
    for(int sdt = 0; sdt < m_sdt; ++sdt){
        for(auto & a : m_subobjects){
            a->solve_constraints();
        }
    }
}







#include "../register.h"
static int unit_test(){
    printc(bc.YELLOW, "Unit Test: Triangle\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(unit_test);






