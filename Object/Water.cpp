#include "Water.h"

#include <math.h>


Water::Water(floatV pos, floatV speed, float radius, Simulation * sim) : Object()
{
    m_sim = sim;
    //m_particles.push_back( new Particle (pos, speed, radius) );
    for(float j = 0; j < 26; ++j){ //12
        for(float i = 0; i < 10; ++i){
            for(float k = 0; k < 10; ++k){
                m_particles.push_back( new Particle(
                    pos + floatV(i * 2.0e-2f, j * 2.0e-2f, k * 2.0e-2f) + floatV(radius * ((int)j % 2), radius, radius * ((int)j % 2)) + floatV(radius, radius, radius),
                    //speed + floatV(i / 10.0f, j / 100.0f),
                    //speed + floatV(k * 2.0e-3f, j * 2.0e-3f, i * 2.0e-3f),
                    speed,
                    radius
                ));
                m_particles.back()->m_type = Particle::default_e;
            }
        }
    }
    m_radius = radius;
}


Water::~Water()
{

}

//Particle * P, {CODE}
#define FOR_ADJECENT_Padj(P, CODE) { \
auto adjecent = m_sim->adjecentcid(*P); \
for(auto adj : adjecent){ \
    int j = m_sim->cells_starts[adj]; \
    for(; j < m_sim->m_data.size() && m_sim->m_data[j]->m_cid == adj; ++j){ \
        Particle * Padj = m_sim->m_data[j]; \
        CODE \
    } \
} \
}

void Water::solve_constraints(){
    float epsilon = 1e2 / ((*sim_dt) * (*sim_dt));
    for(int sdt = 0; sdt < 2; ++sdt){
        #pragma omp parallel for firstprivate(m_particles) schedule(dynamic,100)
        for(auto Pi : m_particles){
            double lambda = 0;
            double gc = 0;
            double c = 0;

            c = C_density(Pi);

            // for(auto Pk : m_particles){
            //     gc += (double) Pk->m_inverse_mass * (double) GC_density(Pi, Pk).lenSq();// + Scorr;
            // }
            FOR_ADJECENT_Padj(Pi, {
                gc += (double) Padj->m_inverse_mass * (double) GC_density(Pi, Padj).lenSq();// + Scorr;
            });
            gc += epsilon;

            Pi->m_lambda = -c / gc;
        }
        #pragma omp parallel for firstprivate(m_particles) schedule(dynamic,100)
        for(auto Pi : m_particles){
            floatV dPi;
            // for(auto Pj : m_particles){
            //     float Scorr = -0.1 * (W(Pi, Pj)/W(0.0008, 0.008 * 1.)) * (W(Pi, Pj)/W(0.0008, 0.008 * 1.)) * (W(Pi, Pj)/W(0.0008, 0.008 * 1.)) * (W(Pi, Pj)/W(0.0008, 0.008 * 1.));
            //     Scorr = 0;
            //     dPi += (Pi->m_lambda + Pj->m_lambda + Scorr) * GPk_density(Pi, Pj) * W(Pi, Pj);
            // }
            FOR_ADJECENT_Padj(Pi, {
                float Scorr = 0;
                dPi += (Pi->m_lambda + Padj->m_lambda + Scorr) * GPk_density(Pi, Padj) * W(Pi, Padj);
            });
            dPi = dPi * (1/Ro0);
            Pi->m_pos += dPi;
        }
    }
}




float Water::C_density(Particle * Pi){
    return Roi(Pi) / Ro0 - 1;
}

float Water::Roi(Particle * Pi){
    float ret = 0;
    // for(auto a : m_particles){
    //     ret += a->m_mass * W(Pi, a);
    // }
    FOR_ADJECENT_Padj(Pi, {
        ret += Padj->m_mass * W(Pi, Padj);
    });
    return ret;
}

float Water::W(float r, float h)
{
    float ret = 1/(pi * h * h * h * h);
    float x = r/h;
    if(0 <= x && x <= 1){
        ret = ret * (9/4) * x*x - 3*x;
    }
    if(1 < x && x <= 2){
        ret = ret * -(3/4) * (2 - x)*(2 - x);
    }
    if(x > 2){
        ret = 0;
    }
    return ret;
}

float Water::W(Particle* Pi, Particle * Pj){
    return W((Pi->m_pos - Pj->m_pos).len(), m_radius * 12.0); //0.1 //0.008 * 1.9
}

floatV Water::GC_density(Particle * Pi, Particle * Pk)
{
    floatV ret;
    if(Pi == Pk){
        // for(auto a : m_particles){
        //     ret += GPk_density(Pk, a) * W(Pk, a);
        // }
        FOR_ADJECENT_Padj(Pi, {
            ret += GPk_density(Pk, Padj) * W(Pk, Padj);
        });
    } else {
        ret = -1 * GPk_density(Pk, Pi) * W(Pk, Pi);
    }
    return ret *  (1 / Ro0);
}



floatV Water::GPk_density(Particle * Pi, Particle* Pk)
{
    //return -1 * (Pi->m_pos + Pi->m_velocity) / (Pi->m_pos + Pi->m_velocity).len();
    //return -1 * (Pk->m_pos + Pk->m_velocity) / (Pk->m_pos + Pk->m_velocity).len();
    if(Pi == Pk)
        return floatV();
    return (Pk->m_pos - Pi->m_pos) / (Pk->m_pos - Pi->m_pos).len();
}




void Water::compute_vorticity()
{
    float c = 0.01;
    for(auto Pi : m_particles){
        floatV sum_j = floatV();
        // for(auto Pj : m_particles){
        //     sum_j += (Pj->m_velocity - Pi->m_velocity) * W(Pi, Pj);
        // }
        FOR_ADJECENT_Padj(Pi, {
            sum_j += (Padj->m_velocity - Pi->m_velocity) * W(Pi, Padj);
        });
        sum_j *= c;
        Pi->m_velocity += sum_j;
    }
}






#include "../register.h"
static int self_test(){
    printc(bc.YELLOW, "Self Test: Water\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(self_test);


