#ifndef CLOTH_H
#define CLOTH_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"


class Cloth : public Object
{
public:
    Cloth(floatV pos, floatV speed, float radius = 1.0e-3);
    ~Cloth() override;
    std::string my_name() override {return "Cloth";};

    void solve_constraints() override;
    void gl_draw() override;

    floatV start_pos;
    // floatV m_position;

    // int size_verts;
    // int size_tetIds;
    // int size_tetEdgeIds;
    // int size_tetSurfaceTriIds;
};











#endif // CONSTRAINT_H
