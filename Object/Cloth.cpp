#include "Cloth.h"

#include<GL/glut.h>

#include <cstdlib>

#include "../Particle/Anchor.h"
#include "Bead.h"

#include "../Raytracer/RayGPU.h"

Cloth::Cloth(floatV pos, floatV speed, float radius) : Object()
{
    start_pos = pos;
    m_sdt = 6;

    int width = 10;
    int height = 20;

    float scale = 64.0;

    for(int y = 0; y < height; ++y){
        for(int x = 0; x < width; ++x){
            if(y == 0 && (x == 0 || x == width-1))
                m_particles.push_back(new Anchor(pos + floatV(x * radius * 6, -y * radius * 6) * scale, floatV(), radius));
            else {
                m_particles.push_back(new Particle(pos + floatV(x * radius * 6, -y * radius * 6) * scale, speed, radius));
                m_particles.back()->m_special_velocity = floatV(0, -9.81, 0);
            }
        }
    }

    printf("\n");
    for(int y = 0; y < height; ++y){
        for(int x = 0; x < width; ++x){
            if(x != width - 1){
                auto tmpx = new Bead(m_particles[x + y * width], m_particles[x + 1 + y * width], 0);
                m_subobjects.push_back(tmpx);
            }
            if(y != height - 1){
                auto tmpy = new Bead(m_particles[x + y * width], m_particles[x + (y + 1) * width], 0);
                m_subobjects.push_back(tmpy);
                if(x != width - 1){
                    float a1;
                    float a2;
                    float bending = 100;
                    int r = rand() % 2;
                    printif(r, "R");
                    if(r){
                       a1 = bending;
                       a2 = 0;
                    } else {
                        a1 = 0;
                        a2 = bending;
                    }
                    auto tmpc1 = new Bead(m_particles[x + y * width], m_particles[x + 1 + (y + 1) * width], a1);
                    m_subobjects.push_back(tmpc1);
                    auto tmpc2 = new Bead(m_particles[x + 1 + y * width], m_particles[x + (y + 1) * width], a2);
                    m_subobjects.push_back(tmpc2);
                }
            }
        }
    }
    printf("\n");
    gl_draw();
}


Cloth::~Cloth()
{

}



void Cloth::solve_constraints()
{
    for(int sdt = 0; sdt < m_sdt; ++sdt)
        for(auto a : m_subobjects)
            a->solve_constraints();
}




static floatV tri_norm(floatV C, floatV Y, floatV Z){
    return (Y - C) ^ (Z - C);
}


void Cloth::gl_draw()
{
    int width = 10;
    int height = 20;


    floatV barva = floatV(1,1,0);
    float matnost = 1;
    bool svetlo = false;

    int obj_count = 0;
    int obj_offset = RayGPU::objekty.size();

    for(int y = 0; y < height-1; ++y){
        for(int x = 0; x < width-1; ++x){
            int body[4] = {x + y * width, x + 1 + y * width, x + (y+1) * width, x+1 + (y+1) * width};

            floatV np0 = m_particles[body[0]]->m_pos - floatV(25,2,25);
            floatV np1 = m_particles[body[1]]->m_pos - floatV(25,2,25);
            floatV np2 = m_particles[body[2]]->m_pos - floatV(25,2,25);

            RayGPU::objekty.push_back(RayGPU::genTriangle(svetlo, barva, matnost, np0, np2, np1));
            RayGPU::objekty.back().bod_ref[0] = &m_particles[body[0]]->m_pos_reference;
            RayGPU::objekty.back().bod_ref[2] = &m_particles[body[1]]->m_pos_reference;
            RayGPU::objekty.back().bod_ref[1] = &m_particles[body[2]]->m_pos_reference;

            np0 = m_particles[body[3]]->m_pos - floatV(25,2,25);
            np1 = m_particles[body[1]]->m_pos - floatV(25,2,25);
            np2 = m_particles[body[2]]->m_pos - floatV(25,2,25);

            RayGPU::objekty.push_back(RayGPU::genTriangle(svetlo, barva, matnost, np0, np1, np2));
            RayGPU::objekty.back().bod_ref[0] = &m_particles[body[3]]->m_pos_reference;
            RayGPU::objekty.back().bod_ref[1] = &m_particles[body[1]]->m_pos_reference;
            RayGPU::objekty.back().bod_ref[2] = &m_particles[body[2]]->m_pos_reference;

            obj_count += 2;
        }
    }

    RayGPU::object ob = RayGPU::genObj(false, barva, matnost, RayGPU::obal_e);
    ob.stred = start_pos + floatV(2,-4, 0);
    ob.r = 8;
    ob.normie = floatV(obj_offset, obj_count, 0);
    // ob.stred_ref = &m_reference;
    RayGPU::obaly.push_back(ob);
}



#include "../register.h"
static int self_test(){
    printc(bc.YELLOW, "Self Test: Cloth\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(unit_test);
