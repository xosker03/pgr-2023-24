#ifndef PENDULUM_H
#define PENDULUM_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"


class Pendulum : public Object
{
public:
    Pendulum(floatV pos, floatV speed, float radius = 1.0e-3);
    ~Pendulum() override;
    std::string my_name() override {return "Pendulum";};

    void solve_constraints() override;

    float _distace = 0.15;
};











#endif // CONSTRAINT_H
