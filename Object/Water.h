#ifndef WATER_H
#define WATER_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"
#include "../Simulation.h"

class Water : public Object
{
public:
    Water(floatV pos, floatV speed, float radius = 1.0e-3, Simulation * sim = NULL);
    ~Water() override;
    std::string my_name() override {return "Water";};

    void solve_constraints() override;
    void compute_vorticity() override;

    float C_density(Particle * Pi);
    float Roi(Particle * Pi);
    float Ro0 = 997 * 1.0;
    float W(float r, float h);
    float W(Particle * Pi, Particle * Pj);
    float pi = 3.1415926;

    floatV GC_density(Particle * Pi, Particle * Pk);
    floatV GPk_density(Particle * Pi, Particle * Pk);

    Simulation * m_sim;

    float m_radius;
};











#endif // CONSTRAINT_H
