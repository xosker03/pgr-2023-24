#ifndef BUNNY_H
#define BUNNY_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"


class Bunny : public Object
{
public:
    Bunny(floatV pos, floatV speed, float radius = 1.0e-3);
    ~Bunny() override;
    std::string my_name() override {return "Bunny";};

    void solve_constraints() override;
    void gl_draw() override;

    floatV m_position;

    int size_verts;
    int size_tetIds;
    int size_tetEdgeIds;
    int size_tetSurfaceTriIds;

    floatV init_pos;
    floatV init_stred;
};











#endif // CONSTRAINT_H
