#include "Cube.h"

#include "Bead.h"

Cube::Cube(floatV pos, floatV speed, float radius)
{
    printf("%s: init\n", my_name().c_str());
    for(int k = 0; k < m_sz; ++k){
        for(int j = 0; j < m_sy; ++j){
            for(int i = 0; i < m_sx; ++i){
                m_particles.push_back( new Particle(
                    pos + floatV(i * radius * 20.0, j * radius * 20.0, k * radius * 20.0),
                    speed,
                    radius
                ));
                m_particles.back()->m_type = Particle::default_e;
            }
        }
    }

    //m_subobjects.push_back(new Bead(m_particles[tetEdgeIds[i][0]], m_particles[tetEdgeIds[i][1]], 100));

    auto isvalid = [this](int i, int j, int k){
        if(i < 0 || i >= m_sx)
            return false;
        if(j < 0 || j >= m_sy)
            return false;
        if(k < 0 || k >= m_sz)
            return false;
        return true;
    };

    auto adj_x_cube = [this, isvalid](int i, int j, int k, int size){
        std::vector<int> ret;
        for(int c = k - size; c <= k + size; ++c){
            for(int b = j - size; b <= j + size; ++b){
                for(int a = i - size; a <= i + size; ++a) {
                    if(isvalid(a, b, c)){
                        ret.push_back(a + b * m_sx + c * m_sx * m_sy);
                    }
                }
            }
        }
        return ret;
    };
    auto adj_x_cross = [this, isvalid](int i, int j, int k, int size){
        std::vector<int> ret;
        for(int c = k - size; c <= k + size; ++c){
            if(isvalid(i, j, c)){
                ret.push_back(i + j * m_sx + c * m_sx * m_sy);
            }
        }
        for(int b = j - size; b <= j + size; ++b){
            if(isvalid(i, b, k)){
                ret.push_back(i + b * m_sx + k * m_sx * m_sy);
            }
        }
        for(int a = i - size; a <= i + size; ++a) {
            if(isvalid(a, j, k)){
                ret.push_back(a + j * m_sx + k * m_sx * m_sy);
            }
        }
        return ret;
    };

    auto adjf = adj_x_cube;
    // auto adjf = adj_x_cross;

    printf("%s: constraints\n", my_name().c_str());
    for(int k = 0; k < m_sz; ++k){
        for(int j = 0; j < m_sy; ++j){
            for(int i = 0; i < m_sx; ++i){
                auto * l = m_particles[i + j * m_sx + k * m_sx * m_sy];
                auto adjecent = adjf(i,j,k, 2);
                for(int a : adjecent){
                    auto * p = m_particles[a];
                    if(l != p)
                        m_subobjects.push_back(new Bead(l, p, 100));
                }
            }
        }
    }
    m_sdt = 10;
}

Cube::~Cube()
{
	
};


void Cube::solve_constraints()
{
    for(int sdt = 0; sdt < m_sdt; ++sdt)
        for(auto a : m_subobjects)
            a->solve_constraints();
}




void Cube::gl_draw()
{
    int idx = (m_sx/2) + (m_sy/2) * m_sx + (m_sz/2) * m_sx * m_sy;
    auto * par = m_particles[idx];
    for(auto a : m_subobjects){
        if(a->m_particles[0] == par)
            a->gl_draw();
    }

    // for(auto a : m_subobjects){
    //     a->gl_draw();
    // }
}


