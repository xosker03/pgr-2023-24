#ifndef Robotarm_H
#define Robotarm_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"

struct RobotArm {
    double L1;
    double L2;
    double L3;
    double q1;
    double q2;
    double q3;
    double x0;
    double y0;
    double x1;
    double y1;
    double x2;
    double y2;
    double x3;
    double y3;
};

struct EndPosition {
    double x;
    double y;
    double fin_ang;
};


class Robotarm : public Object
{
public:
    Robotarm(floatV pos, floatV speed, float radius = 1.0e-3);
	~Robotarm();
    std::string my_name() override {return "RobotArm";};

    void solve_constraints() override;
    void gl_draw() override;

    struct RobotArm arm = {0.2, 0.3, 0.2, -20 * M_PI / 180, 40 * M_PI / 180, 20 * M_PI / 180, 0, 0, 0, 0, 0, 0, 0, 0};
    struct EndPosition target = {2.0, 2.0, 90*M_PI/180}; // Change this to your desired end position

    double learning_rate = 0.02;
    double delta = 0.01;

};


#endif

