#ifndef TETRAHEDRON_H
#define TETRAHEDRON_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"


class Tetrahedron : public Object
{
public:
    Tetrahedron(floatV pos, floatV speed, float radius = 1.0e-3);
    Tetrahedron(Particle * p0, Particle * p1 ,Particle * p2, Particle * p3, float alpha = 0.0);
    ~Tetrahedron() override;
    std::string my_name() override {return "Tetrahedron";};

    void solve_constraints() override;
    void gl_draw() override;

    floatV m_position;

    int size_verts;
    int size_tetIds;
    int size_tetEdgeIds;
    int size_tetSurfaceTriIds;

    int tetEdgeIds[6][2] = {{0,1}, {1,2}, {2,0}, {0,3}, {1,3}, {2,3}};
    // float c_distance_lens[6];

    float c_tetra_volume;
};











#endif // CONSTRAINT_H
