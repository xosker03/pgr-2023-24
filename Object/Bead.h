#ifndef BEAD_H
#define BEAD_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"


class Bead : public Object
{
public:
    Bead(floatV pos, floatV speed, float radius = 1.0e-3);
    Bead(Particle * p0, Particle * p1, float alpha = 0);
    ~Bead() override;
    std::string my_name() override {return "Bead";};

    void init_constraints() override;
    void solve_constraints() override;
    void gl_draw() override;

    float c_distance = 0.0f;
};











#endif // CONSTRAINT_H
