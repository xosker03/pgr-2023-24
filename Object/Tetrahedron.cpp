#include "Tetrahedron.h"

#include<GL/glut.h>

#include "Bead.h"

Tetrahedron::Tetrahedron(floatV pos, floatV speed, float radius) : Object()
{
    m_position = pos;

    // m_particles.push_back( Particle(pos + floatV(0.0, 0.08, 0.00), speed, radius) ); //p1
    // m_particles.push_back( Particle(pos + floatV(-0.06, 0.0, 0.06), speed, radius) ); //p2
    // m_particles.push_back( Particle(pos + floatV(0.06, 0.0, 0.06), speed, radius) ); //p3
    // m_particles.push_back( Particle(pos + floatV(0.0, 0.0, -0.08), speed, radius) ); //p4

    m_particles.push_back( new Particle(pos + floatV(0.0, 0.0, 0.0), speed, radius) ); //p1
    m_particles.push_back( new Particle(pos + floatV(0.2, 0.0, 0.0), speed, radius) ); //p2
    m_particles.push_back( new Particle(pos + floatV(0.1, 0.2, 0.0), speed, radius) ); //p3
    m_particles.push_back( new Particle(pos + floatV(0.1, 0.1, 0.2), speed, radius) ); //p4



    for(int i = 0; i < 6; ++i){
        // c_distance_lens[i] = (m_particles[tetEdgeIds[i][0]]->m_pos - m_particles[tetEdgeIds[i][1]]->m_pos).len();
        m_subobjects.push_back(new Bead(m_particles[tetEdgeIds[i][0]], m_particles[tetEdgeIds[i][1]], 100));
    }

    c_tetra_volume = (1.0/6.0) * (((m_particles[1]->m_pos - m_particles[0]->m_pos) ^ (m_particles[2]->m_pos - m_particles[0]->m_pos)) & (m_particles[3]->m_pos - m_particles[0]->m_pos));
}

Tetrahedron::Tetrahedron(Particle * p0, Particle * p1 ,Particle * p2, Particle * p3, float alpha){
    c_alpha = alpha;
    pseudo_object = true;
    m_sdt = 1;

    m_particles.push_back(p0);
    m_particles.push_back(p1);
    m_particles.push_back(p2);
    m_particles.push_back(p3);

    for(int i = 0; i < 6; ++i){
        // c_distance_lens[i] = (m_particles[tetEdgeIds[i][0]]->m_pos - m_particles[tetEdgeIds[i][1]]->m_pos).len();
        m_subobjects.push_back(new Bead(m_particles[tetEdgeIds[i][0]], m_particles[tetEdgeIds[i][1]], 100));
    }

    c_tetra_volume = (1.0/6.0) * (((m_particles[1]->m_pos - m_particles[0]->m_pos) ^ (m_particles[2]->m_pos - m_particles[0]->m_pos)) & (m_particles[3]->m_pos - m_particles[0]->m_pos));

}


Tetrahedron::~Tetrahedron()
{

}


// static float C_distace(floatV X, floatV Y){
//     float len = (Y - X).len();
//     return len;
// }
// static floatV GC_distace(floatV X, floatV Y){
//     return (Y - X) / (Y - X).len();
// }


static float C_volume(floatV p0, floatV p1, floatV p2, floatV p3){
    return (((p1 - p0) ^ (p2 - p0)) & (p3 - p0));
}
static floatV GC_volume(floatV C, floatV Y, floatV Z){
    return (Y - C) ^ (Z - C);
}



void Tetrahedron::solve_constraints()
{
    // printf("\n");
    // float alpha = 100.0 / ((*sim_dt) * (*sim_dt));
    float alpha2 = c_alpha / ((*sim_dt) * (*sim_dt));

    for(int sdt = 0; sdt < m_sdt; ++sdt){
        for(auto & a : m_subobjects){
            a->solve_constraints();
        }

        // printf("\n");
        Particle * p0 = m_particles[0];
        Particle * p1 = m_particles[1];
        Particle * p2 = m_particles[2];
        Particle * p3 = m_particles[3];
        float lambda = 0;
        float gc = 0;
        float c = 0;
        c = (C_volume(p0->m_pos, p1->m_pos, p2->m_pos, p3->m_pos) - c_tetra_volume * 6.0) ;
        gc += p0->m_inverse_mass * GC_volume(p1->m_pos, p3->m_pos, p2->m_pos).lenSq();
        gc += p1->m_inverse_mass * GC_volume(p0->m_pos, p2->m_pos, p3->m_pos).lenSq();
        gc += p2->m_inverse_mass * GC_volume(p0->m_pos, p3->m_pos, p1->m_pos).lenSq();
        gc += p3->m_inverse_mass * GC_volume(p0->m_pos, p1->m_pos, p2->m_pos).lenSq();
        gc += alpha2;
        //printf("%f %f", C_volume(p0->m_pos, p1->m_pos, p2->m_pos, p3->m_pos), c_tetra_volume);
        if(gc != 0.0){
            lambda = -c / gc;
            floatV dp0 = lambda * p0->m_inverse_mass * GC_volume(p1->m_pos, p3->m_pos, p2->m_pos);
            floatV dp1 = lambda * p1->m_inverse_mass * GC_volume(p0->m_pos, p2->m_pos, p3->m_pos);
            floatV dp2 = lambda * p2->m_inverse_mass * GC_volume(p0->m_pos, p3->m_pos, p1->m_pos);
            floatV dp3 = lambda * p3->m_inverse_mass * GC_volume(p0->m_pos, p1->m_pos, p2->m_pos);
            p0->m_pos += dp0;
            p1->m_pos += dp1;
            p2->m_pos += dp2;
            p3->m_pos += dp3;
        }
        //printf("\n");
    }
}



void Tetrahedron::gl_draw()
{
    //glColor3f(0.0, 0.0, 1.0);
    glBegin(GL_TRIANGLE_STRIP);
        glColor3f(1, 1, 1); glVertex3f(m_particles[0]->m_pos.x, m_particles[0]->m_pos.y, m_particles[0]->m_pos.z);
        glColor3f(1, 0, 0); glVertex3f(m_particles[1]->m_pos.x, m_particles[1]->m_pos.y, m_particles[1]->m_pos.z);
        glColor3f(0, 1, 0); glVertex3f(m_particles[2]->m_pos.x, m_particles[2]->m_pos.y, m_particles[2]->m_pos.z);
        glColor3f(0, 0, 1); glVertex3f(m_particles[3]->m_pos.x, m_particles[3]->m_pos.y, m_particles[3]->m_pos.z);
        glColor3f(1, 1, 1); glVertex3f(m_particles[0]->m_pos.x, m_particles[0]->m_pos.y, m_particles[0]->m_pos.z);
        glColor3f(1, 0, 0); glVertex3f(m_particles[1]->m_pos.x, m_particles[1]->m_pos.y, m_particles[1]->m_pos.z);
    glEnd();

}



#include "../register.h"
static int unit_test(){
    printc(bc.YELLOW, "Unit Test: Tetrahedron\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(unit_test);

