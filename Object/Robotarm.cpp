#include "Robotarm.h"

#include<GL/glut.h>

Robotarm::Robotarm(floatV pos, floatV speed, float radius)  : Object()
{
	m_particles.push_back(new Particle(pos, speed, radius));
}

Robotarm::~Robotarm()
{
	
}



double evaluateCost(struct RobotArm arm, struct EndPosition target) {
    // Evaluate the cost function based on the robot arm's end position and the target
    double a = (arm.L1* cos(arm.q1)
        + arm.L2 * cos(arm.q1+arm.q2)
        + arm.L3 * cos(arm.q1+arm.q2+arm.q3)
    )-target.x;
    double b = (arm.L1* sin(arm.q1)
        + arm.L2 * sin(arm.q1+arm.q2)
        + arm.L3 * sin(arm.q1+arm.q2+arm.q3)
    )-target.y;
    double c = (arm.q1 + arm.q2 + arm.q3 - target.fin_ang);
    return 0.5 * a*a
    + 0.5 * b*b;
    //+ 0.5 * c*c;
}

void computeGradients(struct RobotArm arm, struct EndPosition target, double delta, double* grad1, double* grad2, double* grad3) {
    // Compute gradients using finite differences
    struct RobotArm arm_delta_q1 = arm;
    struct RobotArm arm_delta_q2 = arm;
    struct RobotArm arm_delta_q3 = arm;

    arm_delta_q1.q1 += delta;
    arm_delta_q2.q2 += delta;
    arm_delta_q3.q3 += delta;

    double cost_orig = evaluateCost(arm, target);
    double cost_delta_q1 = evaluateCost(arm_delta_q1, target);
    double cost_delta_q2 = evaluateCost(arm_delta_q2, target);
    double cost_delta_q3 = evaluateCost(arm_delta_q3, target);

    *grad1 = (cost_delta_q1 - cost_orig) / delta;
    *grad2 = (cost_delta_q2 - cost_orig) / delta;
    *grad3 = (cost_delta_q3 - cost_orig) / delta;
}

void updateAngles(struct RobotArm *arm, double learning_rate, double grad1, double grad2, double grad3) {
    // Update angles using gradients and learning rate
    arm->q1 -= learning_rate * grad1;
    arm->q2 -= learning_rate * grad2;
    arm->q3 -= learning_rate * grad3;
}

void forwardKinematics(struct RobotArm *arm) {
    // Perform forward kinematics to update the positions of each joint
    arm->x1 = arm->L1 * cos(arm->q1);
    arm->y1 = arm->L1 * sin(arm->q1);
    arm->x2 = arm->x1 + arm->L2 * cos(arm->q1 + arm->q2);
    arm->y2 = arm->y1 + arm->L2 * sin(arm->q1 + arm->q2);
    arm->x3 = arm->x2 + arm->L3 * cos(arm->q1 + arm->q2 + arm->q3);
    arm->y3 = arm->y2 + arm->L3 * sin(arm->q1 + arm->q2 + arm->q3);
}

void showRobotArm(struct RobotArm arm, struct EndPosition target) {
    // printf("Joint positions:\n");
    // printf("Joint 1: (%f, %f)\n", arm.x1, arm.y1);
    // printf("Joint 2: (%f, %f)\n", arm.x2, arm.y2);
    // printf("End Effector (Joint 3): (%f, %f)\n", arm.x3, arm.y3);
    // printf("End Pos: (%f, %f)\n", target.x, target.y);
}




void Robotarm::solve_constraints()
{
    target.x = m_particles[0]->m_pos.x;
    target.y = m_particles[0]->m_pos.y;

    for (int iter = 1; iter <= 60; iter++) {
        forwardKinematics(&arm);

        double grad1, grad2, grad3;
        computeGradients(arm, target, delta, &grad1, &grad2, &grad3);
        updateAngles(&arm, learning_rate, grad1, grad2, grad3);

        // printf("Iteration: %d\n", iter);
        showRobotArm(arm, target);
    }
}



void Robotarm::gl_draw()
{
    glColor3f(0.0, 0.9, 0.9);
    glBegin(GL_LINES);
    glVertex3f(arm.x0, arm.y0, 0.2);
    glVertex3f(arm.x1, arm.y1, 0.2);
    glEnd();

    glBegin(GL_LINES);
    glVertex3f(arm.x1, arm.y1, 0.2);
    glVertex3f(arm.x2, arm.y2, 0.2);
    glEnd();

    glBegin(GL_LINES);
    glVertex3f(arm.x2, arm.y2, 0.2);
    glVertex3f(arm.x3, arm.y3, 0.2);
    glEnd();
}



