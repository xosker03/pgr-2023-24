#ifndef BALL_H
#define BALL_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"


class Ball : public Object
{
public:
    Ball(floatV pos, floatV speed, float radius = 1.0e-3);
    Ball(Particle * p0);
    ~Ball() override;
    std::string my_name() override {return "Ball";};

    void init_constraints() override;
    void solve_constraints() override;
};











#endif // CONSTRAINT_H
