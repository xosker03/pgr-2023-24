#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"


class Triangle : public Object
{
public:
    Triangle(floatV pos, floatV speed, float radius = 1.0e-3);
    Triangle(Particle * p0, Particle * p1, Particle * p2, float alpha = 0);
    ~Triangle() override;
    std::string my_name() override {return "Triangle";};

    // float Ct(floatV A, floatV B, floatV C);
    // floatV GCt(floatV A, floatV B, floatV C);

    void solve_constraints() override;

    // float m_obsah;
    Particle * A;
    Particle * B;
    Particle * C;
};











#endif // CONSTRAINT_H
