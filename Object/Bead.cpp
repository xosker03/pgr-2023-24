#include "Bead.h"

#include<GL/glut.h>

Bead::Bead(floatV pos, floatV speed, float radius) : Object()
{
    m_particles.push_back( new Particle (pos, speed, radius, Particle::koralek_e) );
    m_particles.push_back( new Particle (pos + floatV(radius * 3, radius * 3, 0), speed, radius, Particle::koralek_e) );
    init_constraints();
}

Bead::Bead(Particle* p0, Particle* p1, float alpha)
{
    c_alpha = alpha;
    pseudo_object = true;
    m_sdt = 1;
    m_particles.push_back(p0);
    m_particles.push_back(p1);
    init_constraints();
}


Bead::~Bead()
{

}


static float C_distace(floatV X, floatV Y){
    float len = (Y - X).len();
    return len;
}
static floatV GC_distace(floatV X, floatV Y){
    return (Y - X) / (Y - X).len();
}


void Bead::init_constraints()
{
    c_distance = C_distace(m_particles[0]->m_pos, m_particles[1]->m_pos);
}

void Bead::solve_constraints(){
    float alpha = c_alpha / ((*sim_dt) * (*sim_dt));
    for(int sdt = 0; sdt < m_sdt; ++sdt){
        for(auto a : m_subobjects)
            a->solve_constraints();

        Particle * A = m_particles[0];
        Particle * B = m_particles[1];
        float lambda = 0;
        float gc = 0;
        float c = 0;
        c = C_distace(A->m_pos, B->m_pos) - c_distance;
        gc = A->m_inverse_mass * GC_distace(A->m_pos, B->m_pos).lenSq() + B->m_inverse_mass * GC_distace(B->m_pos, A->m_pos).lenSq() + alpha;
        // printf("%f %f", C_distace(A->m_pos, B->m_pos), c_distance_lens[i]);
        // printgreen(" %f %f", c, gc);
        if(gc != 0.0){
            lambda = -c / gc;
            floatV dA = lambda * A->m_inverse_mass * GC_distace(A->m_pos, B->m_pos);
            floatV dB = lambda * B->m_inverse_mass * GC_distace(B->m_pos, A->m_pos);
            A->m_pos -= dA;
            B->m_pos -= dB;
            // printred("\t %f %f %f", lambda, dA.len(), dB.len());
        }
        // printf("\n");
    }
}



void Bead::gl_draw()
{
    glColor3f(0.0, 0.4, 0.0);
    glBegin(GL_LINES);
        glVertex3f(m_particles[0]->m_pos.x, m_particles[0]->m_pos.y, m_particles[0]->m_pos.z);
        glVertex3f(m_particles[1]->m_pos.x, m_particles[1]->m_pos.y, m_particles[1]->m_pos.z);
    glEnd();
}



#include "../register.h"
static int unit_test(){
    printc(bc.YELLOW, "Unit Test: Bead\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(unit_test);
