#include "Ball.h"



Ball::Ball(floatV pos, floatV speed, float radius) : Object()
{
    m_particles.push_back( new Particle (pos, speed, radius) );
    m_particles.back()->R = ((double) rand() / (RAND_MAX));
    m_particles.back()->G = ((double) rand() / (RAND_MAX));
    m_particles.back()->B = ((double) rand() / (RAND_MAX));
    m_particles.back()->m_type = Particle::default_e;
}

Ball::Ball(Particle* p0)
{
    pseudo_object = true;
    m_particles.push_back(p0);
    m_sdt = 1;
}

Ball::~Ball()
{

}

void Ball::init_constraints()
{

}

void Ball::solve_constraints()
{

}



#include "../register.h"
static int unit_test(){
    printc(bc.YELLOW, "Unit Test: Ball\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(unit_test);
