#include "Bunny.h"

#include "bunny_mesh.h"

#include<GL/glut.h>

#include "Tetrahedron.h"

#include "../Raytracer/RayGPU.h"

#include "assert.h"

Bunny::Bunny(floatV pos, floatV speed, float radius) : Object()
{
    //m_particles.push_back( Particle (pos, speed, radius) );
    float scale = 4.0;

    size_verts = sizeof(verts) / sizeof(float);
    size_tetIds = sizeof(tetIds) / sizeof(int);
    size_tetEdgeIds = sizeof(tetEdgeIds) / sizeof(int);
    size_tetSurfaceTriIds = sizeof(tetSurfaceTriIds) / sizeof(int);

    // printf("verts: %d\n", size_verts);
    // printf("tetIds: %d\n", size_tetIds);
    // printf("tetEdgeIds: %d\n", size_tetEdgeIds);
    // printf("tetSurfaceTriIds: %d\n", size_tetSurfaceTriIds);

    m_position = pos;

    for(int i = 0; i < (size_verts -2); i += 3){
        floatV np = pos + floatV(verts[i], verts[i+1], verts[i+2]) * scale;
        m_particles.push_back( new Particle(np, speed, radius) );
        if(i < 10){
            m_particles.back()->m_initial_speed = floatV(125,125,125);
        }
        if(i > size_verts -12){
            m_particles.back()->m_initial_speed = floatV(-125,-125,-125);
        }
    }

    for(int i = 0; i < (size_tetIds - 3); i += 4){
        int body[4] = {tetIds[i], tetIds[i+1], tetIds[i+2], tetIds[i+3]};
        m_subobjects.push_back(new Tetrahedron(m_particles[body[0]], m_particles[body[1]], m_particles[body[2]], m_particles[body[3]]));
    }

    m_sdt = 6;

    {
        floatV barva = floatV(1,1,1);
        float matnost = 1;
        bool svetlo = false;

        int size_verts = sizeof(verts) / sizeof(float);
        int size_tetSurfaceTriIds = sizeof(tetSurfaceTriIds) / sizeof(int);
        floatV stred;
        float polomer = 0;
        int obj_count = 0;
        int obj_offset = RayGPU::objekty.size();
        for(int i = 0; i < (size_tetSurfaceTriIds - 3); i += 3){
            int body[3] = {tetSurfaceTriIds[i], tetSurfaceTriIds[i+1], tetSurfaceTriIds[i+2]};
            floatV np0 = pos + floatV(verts[body[0]*3], verts[body[0]*3+1], verts[body[0]*3+2]) * scale;
            floatV np1 = pos + floatV(verts[body[1]*3], verts[body[1]*3+1], verts[body[1]*3+2]) * scale;
            floatV np2 = pos + floatV(verts[body[2]*3], verts[body[2]*3+1], verts[body[2]*3+2]) * scale;
            stred += floatV(verts[body[0]*3], verts[body[0]*3+1], verts[body[0]*3+2])
            + floatV(verts[body[1]*3], verts[body[1]*3+1], verts[body[1]*3+2])
            + floatV(verts[body[2]*3], verts[body[2]*3+1], verts[body[2]*3+2]);
            obj_count++;
            RayGPU::objekty.push_back(RayGPU::genTriangle(svetlo, barva, matnost, np0, np2, np1));
            RayGPU::objekty.back().bod_ref[0] = &m_particles[body[0]]->m_pos_reference;
            RayGPU::objekty.back().bod_ref[2] = &m_particles[body[1]]->m_pos_reference;
            RayGPU::objekty.back().bod_ref[1] = &m_particles[body[2]]->m_pos_reference;
            // assert(body[0] < m_particles.size());
            // assert(body[1] < m_particles.size());
            // assert(body[2] < m_particles.size());
            // printf("*ref %p %p %p\n", &m_particles[body[0]]->m_pos_reference, &m_particles[body[1]]->m_pos_reference, &m_particles[body[2]]->m_pos_reference);
        }
        stred /= obj_count;
        for(int i = obj_offset; i < obj_offset + obj_count; ++i){
            int body[3] = {tetSurfaceTriIds[i], tetSurfaceTriIds[i+1], tetSurfaceTriIds[i+2]};
            floatV np0 = floatV(verts[body[0]*3], verts[body[0]*3+1], verts[body[0]*3+2]);
            floatV np1 = floatV(verts[body[1]*3], verts[body[1]*3+1], verts[body[1]*3+2]);
            floatV np2 = floatV(verts[body[2]*3], verts[body[2]*3+1], verts[body[2]*3+2]);
            polomer = fmax(polomer, (np0 - stred).len());
            polomer = fmax(polomer, (np1 - stred).len());
            polomer = fmax(polomer, (np2 - stred).len());
        }
        polomer *= scale * 0.8;
        stred += pos;
        // stred += floatV(0, -0.25 * scale, 0);

        RayGPU::object ob = RayGPU::genObj(false, barva, matnost, RayGPU::obal_e);
        ob.stred = stred;
        ob.r = polomer;
        ob.normie = floatV(obj_offset, obj_count, 0);
        ob.stred_ref = &m_reference;
        RayGPU::obaly.push_back(ob);
        // RayGPU::obaly.back().stred_ref = &m_reference;
        init_stred = stred;
    }

    init_pos = m_particles[m_particles.size() / 2]->m_pos;
}


Bunny::~Bunny()
{

}



void Bunny::solve_constraints()
{
    for(int sdt = 0; sdt < m_sdt; ++sdt)
        for(auto a : m_subobjects)
            a->solve_constraints();

    // floatV stred;
    // int obj_count = 0;
    // for(int i = 0; i < (size_tetSurfaceTriIds - 3); i += 3){
    //     int body[3] = {tetSurfaceTriIds[i], tetSurfaceTriIds[i+1], tetSurfaceTriIds[i+2]};
    //
    //     floatV np0 = m_particles[body[0]]->m_pos;
    //     floatV np1 = m_particles[body[1]]->m_pos;
    //     floatV np2 = m_particles[body[2]]->m_pos;
    //     stred += np0 + np1 + np2;
    //     obj_count++;
    // }
    // stred /= obj_count;
    for(auto & a : m_reference){
        *a = init_stred + (m_particles[m_particles.size() / 2]->m_pos - init_pos);
    }
}




static floatV tri_norm(floatV C, floatV Y, floatV Z){
    return (Y - C) ^ (Z - C);
}


void Bunny::gl_draw()
{
    // glPushMatrix();
    //glTranslated(m_position.x, m_position.y, m_position.z);

    // for(int i = 0; i < (size_tetIds - 3); i += 4){
    //     int body[4] = {tetIds[i], tetIds[i+1], tetIds[i+2], tetIds[i+3]};
    //     glColor3f(0.0, 0.0, 1.0 * i/size_tetIds);
    //     glBegin(GL_TRIANGLE_STRIP);
    //         glVertex3f(m_particles[body[0]]->m_pos.x, m_particles[body[0]]->m_pos.y, m_particles[body[0]]->m_pos.z);
    //         glVertex3f(m_particles[body[1]]->m_pos.x, m_particles[body[1]]->m_pos.y, m_particles[body[1]]->m_pos.z);
    //         glVertex3f(m_particles[body[2]]->m_pos.x, m_particles[body[2]]->m_pos.y, m_particles[body[2]]->m_pos.z);
    //         glVertex3f(m_particles[body[3]]->m_pos.x, m_particles[body[3]]->m_pos.y, m_particles[body[3]]->m_pos.z);
    //         glVertex3f(m_particles[body[0]]->m_pos.x, m_particles[body[0]]->m_pos.y, m_particles[body[0]]->m_pos.z);
    //         glVertex3f(m_particles[body[1]]->m_pos.x, m_particles[body[1]]->m_pos.y, m_particles[body[1]]->m_pos.z);
    //     glEnd();
    // }

    // for(int i = 0; i < (size_tetEdgeIds - 1); i += 2){
    //     int body[2] = {tetEdgeIds[i], tetEdgeIds[i+1]};
    //     glColor3f(0.0, 0.4, 0.0);
    //     glBegin(GL_LINES);
    //         glVertex3f(m_particles[body[0]]->m_pos.x, m_particles[body[0]]->m_pos.y, m_particles[body[0]]->m_pos.z);
    //         glVertex3f(m_particles[body[1]]->m_pos.x, m_particles[body[1]]->m_pos.y, m_particles[body[1]]->m_pos.z);
    //     glEnd();
    // }

//     for(int i = 0; i < (size_tetSurfaceTriIds - 3); i += 3){
//         int body[3] = {tetSurfaceTriIds[i], tetSurfaceTriIds[i+1], tetSurfaceTriIds[i+2]};
//
//         floatV sun = floatV(2,2,2);
//         floatV normie = tri_norm(m_particles[body[0]]->m_pos, m_particles[body[1]]->m_pos, m_particles[body[2]]->m_pos);
//         sun = sun / sun.len();
//         normie = normie / normie.len();
//         float shines = sun & normie;
//         //printf("%f %s %s\n", shines, sun.str().c_str(), normie.str().c_str());
//
//         glColor3f(0.8 * shines + 0.2, 0.0, 0.2 * -shines);
//         glBegin(GL_POLYGON);
//             glVertex3f(m_particles[body[0]]->m_pos.x, m_particles[body[0]]->m_pos.y, m_particles[body[0]]->m_pos.z);
//             glVertex3f(m_particles[body[1]]->m_pos.x, m_particles[body[1]]->m_pos.y, m_particles[body[1]]->m_pos.z);
//             glVertex3f(m_particles[body[2]]->m_pos.x, m_particles[body[2]]->m_pos.y, m_particles[body[2]]->m_pos.z);
//         glEnd();
//     }
    //
    //
    // glPopMatrix();
}



#include "../register.h"
static int unit_test(){
    printc(bc.YELLOW, "Unit Test: Bunny\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(unit_test);
