#ifndef Cube_H
#define Cube_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "../Object.h"

class Cube : public Object
{
public:
    Cube(floatV pos, floatV speed, float radius = 1.0e-3);
	~Cube() override;
    std::string my_name() override {return "Cube";};

    void solve_constraints() override;
    void gl_draw() override;

    int m_sx = 7;
    int m_sy = 7;
    int m_sz = 7;
};


#endif

