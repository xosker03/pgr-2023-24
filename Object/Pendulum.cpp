#include "Pendulum.h"

#include <limits>

#include "../Particle/Anchor.h"
#include "Bead.h"

Pendulum::Pendulum(floatV pos, floatV speed, float radius) : Object()
{
    m_particles.push_back( new Anchor (pos, floatV(0,0), radius, Particle::kyvadlo_e) );
    m_particles.push_back( new Particle (pos + floatV(0.15, 0, 0.0), floatV(0,0), radius, Particle::kyvadlo_e) );
    m_particles.push_back( new Particle (pos + floatV(0.15, 0.15, 0.0), floatV(0,0), radius, Particle::kyvadlo_e) );
    m_particles.push_back( new Particle (pos + floatV(0.30, 0.15, 0.0), speed, radius, Particle::kyvadlo_e) );

    m_subobjects.push_back(new Bead(m_particles[0], m_particles[1]));
    m_subobjects.push_back(new Bead(m_particles[1], m_particles[2]));
    m_subobjects.push_back(new Bead(m_particles[2], m_particles[3]));
}


Pendulum::~Pendulum()
{

}


void Pendulum::solve_constraints(){
    for(int sdt = 0; sdt < m_sdt; ++sdt){
        for(auto & a : m_subobjects){
            a->solve_constraints();
        }
    }
}



#include "../register.h"
static int unit_test(){
    printc(bc.YELLOW, "Unit Test: Pendulum\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(unit_test);
