#ifndef DENGINE_H
#define DENGINE_H

#include <tuple>
#include <vector>

#include <GL/glut.h>

#include "floatV.h"

namespace Dengine
{
    void Glengine(int argc, char** argv, int width = 640, int height = 360);
    void reshape(GLint w, GLint h);
    void mainLoop();
    void display();
    void redisplay();
    void update();
    // void timer(int v);
    void keyPress(unsigned char key, int x, int y);
    void mousePress(int button, int state, int x, int y);
    void mouseMotion(int x, int y);

    void setCamera();
    void rotateCamera();
    void computeCameraAngle(int x, int y);

    void drawLine(floatV from, floatV to, floatV color);
    void drawSphere(floatV from, float radius, floatV color);
};

#endif // GLENGINE_H
