#ifndef FLOATM_H
#define FLOATM_H

#include <stdlib.h>
#include <string.h>

#include "floatV.h"

struct floatM3 {
    inline floatM3(){
        memset(data, 0, 3*3*sizeof(float));
    }

    inline floatM3(float a11, float a12, float a13,
                   float a21, float a22, float a23,
                   float a31, float a32, float a33)
    {
        data[0][0] = a11; data[0][1] = a12; data[0][2] = a13;
        data[1][0] = a21; data[1][1] = a22; data[1][2] = a23;
        data[2][0] = a31; data[2][1] = a32; data[2][2] = a33;
    }

    inline floatM3(const floatM3& other) {
        for(int j = 0; j < 3; ++j)
            for(int i = 0; i < 3; ++i)
                data[j][i] = other.data[j][i];
    }

    //přiřazení
    inline floatM3& operator=(const floatM3& source) {
        memcpy(data, source.data, 3*3*sizeof(float));
        return *this;
    }

    //maticové sčítání
    inline floatM3 operator+(const floatM3& right) const {
        floatM3 ret;
        for(int j = 0; j < 3; ++j)
            for(int i = 0; i < 3; ++i)
                ret.data[j][i] = data[j][i] + right.data[j][i];
        return ret;
    }

    //maticový součin s vektorem
    inline friend floatV operator^(const floatV& l, const floatM3& r) {
        floatV ret;
        ret.x = r.data[0][0] * l.x + r.data[0][1] * l.y + r.data[0][2] * l.z;
        ret.y = r.data[1][0] * l.x + r.data[1][1] * l.y + r.data[1][2] * l.z;
        ret.z = r.data[2][0] * l.x + r.data[2][1] * l.y + r.data[2][2] * l.z;
        return ret;
    }

    inline float & a11(){return data[0][0];}
    inline float & a12(){return data[0][1];}
    inline float & a13(){return data[0][2];}
    inline float & a21(){return data[1][0];}
    inline float & a22(){return data[1][1];}
    inline float & a23(){return data[1][2];}
    inline float & a31(){return data[2][0];}
    inline float & a32(){return data[2][1];}
    inline float & a33(){return data[2][2];}

    float data [3][3] = {};
};



#endif
