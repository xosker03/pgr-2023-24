#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <string>

#include "GLEngine.h"
#include "Dengine.h"

#include "floatV.h"

using namespace std;

volatile float * sim_dt;

int main (int argc, char** argv){

    floatV_self_test();

    int resw = 640;
    int resh = 360;

    if(argc > 1){
        resw = strtof(argv[1], NULL);
        resh = resw * 9 / 16;
    }

    // if(resw % 32 || resh % 32){
    //     printf("špatné rozlišení\n");
    //     return 1;
    // }

    GLEngine ge(resw, resh);
    ge.mainLoop();


    // Dengine::Glengine(argc, argv, 256, 256*9/16);
    // Dengine::mainLoop();


	return 0;
}



