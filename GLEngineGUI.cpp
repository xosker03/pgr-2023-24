#include "GLEngine.h"



void GLEngine::guiNewFrame()
{
    // (Your code calls glfwPollEvents())
    // ...
    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}



void GLEngine::guiUpdate()
{
    // ImGui::ShowDemoWindow(); // Show demo window! :)

    // if (show_demo_window)
    //     ImGui::ShowDemoWindow(&show_demo_window);

    // 2. Show a simple window that we create ourselves. We use a Begin/End pair to create a named window.
    {
        static int counter = 0;

        ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.
        ImGui::Checkbox("Animation", &m_simulate);
        ImGui::Checkbox("Raytracer", &m_run_raytracer);
        if (ImGui::Button("Switch")){
            rayEngine.m_sett.rrtracing = !rayEngine.m_sett.rrtracing;
            RayGPU::recompile(rayEngine.m_sett.rrtracing);
        }
        ImGui::SameLine();
        if (ImGui::Button("Recompile")){
            RayGPU::recompile(rayEngine.m_sett.rrtracing);
        }
        ImGui::SameLine(); ImGui::Text("Tracer = %d", rayEngine.m_sett.rrtracing);

        // ImGui::Checkbox("Real Ray", &rayEngine.m_sett.rrtracing);
        ImGui::SliderInt("Ray Bounces", &rayEngine.m_sett.ray_bounces, 1, 8);
        ImGui::SliderInt("Cycles", &rayEngine.m_sett.work_group_cycles, 1, 8);
        ImGui::SliderInt("Average", &rayEngine.m_sett.average_render_max, 0, 99);
        if(m_stop_on_average){
            if (ImGui::Button("Stop ON"))
                m_stop_on_average = false;
        } else {
            if (ImGui::Button("Stop OFF"))
                m_stop_on_average = true;
        }
        ImGui::SameLine();
        ImGui::Text("Actual average = %d", rayEngine.m_sett.average_render);
        if (ImGui::Button("Reset Frames"))
            counter = 0;
        ImGui::SameLine(); ImGui::Text("Frame = %d", counter);

        ImGui::Text("Ostatní");
        ImGui::Checkbox("Party", &rayEngine.m_sett.party);
        // ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
        // ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
        ImGui::Checkbox("Camera Window", &show_another_window);
        ImGui::Checkbox("Render points", &render_points);

        // ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

        // if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
        //     counter++;
        // ImGui::SameLine();
        // ImGui::Text("counter = %d", counter);

        ImGui::Text("Application %.3f ms (%.1f FPS)", 1000.0f / io->Framerate, io->Framerate);
        ImGui::Text("Render      %.3f ms (%.1f FPS)", timRedner.ms(), 1000.0f /  timRedner.ms());
        // ImGui::Text("Raytracing  %.3f ms (%.1f FPS)", rayEngine.tim.ms(), 1000.0f /  rayEngine.tim.ms());
        ImGui::Text("Objektu: %d", num_objs);


        if(m_stop_on_average && rayEngine.m_sett.average_render == rayEngine.m_sett.average_render_max){
            m_run_raytracer = false;
        }
        if(counter <= rayEngine.m_sett.average_render_max)
            rayEngine.m_sett.average_render = counter;

        if(m_run_raytracer)
            counter++;
        ImGui::End();
    }

    // 3. Show another simple window.
    if (show_another_window)
    {
        ImGui::Begin("Camera Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
        // ImGui::Text("Hello from another window!");

        ImGui::SliderFloat("Fov", &rayEngine.m_fov, 0.01f, 2.0f);

        ImGui::SliderFloat("cam star x", &rayEngine.cam.start.x, -10.0f, 10.0f);
        ImGui::SliderFloat("cam star y", &rayEngine.cam.start.y, -10.0f, 10.0f);
        ImGui::SliderFloat("cam star z", &rayEngine.cam.start.z, -10.0f, 10.0f);

        ImGui::SliderFloat("cam dir x", &rayEngine.cam.p.x, -1.0f, 1.0f);
        ImGui::SliderFloat("cam dir y", &rayEngine.cam.p.y, -1.0f, 1.0f);
        ImGui::SliderFloat("cam dir z", &rayEngine.cam.p.z, -1.0f, 1.0f);

        if (ImGui::Button("Close Me"))
            show_another_window = false;

        ImGui::End();
    }
}


void GLEngine::guiRender()
{
    // Rendering
    // (Your code clears your framebuffer, renders your other stuff etc.)
    ImGui::Render();
    // (Your code calls glfwSwapBuffers() etc.)
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}





