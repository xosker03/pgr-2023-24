#include "GLEngine.h"

class GLEngine * ENGINE;

static GLuint viewUniform;
static GLuint projUniform;
static GLuint sunUniform;
static glm::mat4 view;
static glm::mat4 proj;
static glm::vec3 sun;
static double kamera = 0;



static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    ENGINE->keyCallback(window, key, scancode, action, mods);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);

    float a = (float) width / (float) height;
    proj = glm::perspective(glm::radians(60.f), a, .1f, 1000.f);
    view = glm::lookAt(glm::vec3(2.f,2.f,3.f), glm::vec3(0.f), glm::vec3(0.f, 1.f, 0.f));
}

void APIENTRY debugCallback(GLenum source,
                   GLenum type,
                   GLuint id,
                   GLenum severity,
                   GLsizei length,
                   const GLchar *message,
                   const void *userParam)
{
    // printred("GL:");
    // printf("%u %u %u %u %u : %s\n", source, type, id, severity, length, message);
    // ignore non-significant error/warning codes
    if(id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

    std::cout << "---------------" << std::endl;
    std::cout << "Debug message (" << id << "): " <<  message << std::endl;

    switch (source)
    {
        case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
        case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
        case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
    } std::cout << std::endl;

    switch (type)
    {
        case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
        case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
        case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
        case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
        case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
        case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
        case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
    } std::cout << std::endl;

    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
        case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
        case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
    } std::cout << std::endl;
    std::cout << std::endl;
}



GLEngine::GLEngine(int ray_width, int ray_height, int gl_width, int gl_height)
{
    ENGINE = this;
    const char* glsl_version = "#version 450";

    //GLFW
    glfwSetErrorCallback(error_callback);
    if(!glfwInit())
        exit(EXIT_FAILURE);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

    // glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
    // GLFWwindow* window = glfwCreateWindow(1920, 1080, "Simple", NULL, NULL);
    window = glfwCreateWindow(gl_width, gl_height, "Simple", NULL, NULL);
    if(!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwSetKeyCallback(window, key_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    //GLEW
    GLenum err = glewInit();
    if(GLEW_OK != err){
        std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
        glfwTerminate();
        throw -1;
        // return -1;
    }

    //OpenGL
    // glClearColor(0.1f,0.2f,0.2f,1.0f);
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    // glEnable(GL_DEPTH_TEST);
    // glEnable(GL_CULL_FACE);
    // glCullFace(GL_BACK);
    if (GLEW_ARB_debug_output) {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(debugCallback, nullptr);
        // glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
    } else {
        std::cerr << "Debug Output Extension not supported!" << std::endl;
    }



    //IMGUI
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    io = &ImGui::GetIO();
    io->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io->ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io->ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    io->ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    //BUFFERS
    buff.lateConstruct(&vertices, &colors, &indices);

    //SHADERS
    // program.compile(GL_GEOMETRY_SHADER, "geometry");
    program.compile_vertex();
    program.compile_fragment();
    program.link();
    program.use();



    viewUniform = glGetUniformLocation(program.m_mainProgram,"view");
    projUniform = glGetUniformLocation(program.m_mainProgram,"proj");
    sunUniform = glGetUniformLocation(program.m_mainProgram,"sun");

    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);
    framebuffer_size_callback(window, display_w, display_h);

    raytracerInit(ray_width, ray_height);
}

GLEngine::~GLEngine()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}



void GLEngine::mainLoop()
{
    while(!glfwWindowShouldClose(window)){
        if(m_run_raytracer)
            timRedner.stop();
        glfwPollEvents();
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        guiNewFrame();
        guiUpdate();
        if(m_run_raytracer)
            timRedner.start();
        // glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);

        ////// Můj kód //////

        newFrame();
        render();

        /////////////////////

        guiRender();
        if (io->ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            GLFWwindow* backup_current_context = glfwGetCurrentContext();
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
            glfwMakeContextCurrent(backup_current_context);
        }
        glfwSwapBuffers(window);
    }
}



void GLEngine::raytracerInit(int width, int height)
{

    rayEngine.lateInit(false, width, height);

    vertices.resize(width * height);
    colors.resize(width * height);

    for(int j = 0; j < height; ++j){
        for(int i = 0; i < width; ++i){
            struct Vertex a;
            struct Point b;
            a.x = i * 2.0 / (width - 1) - 1.0;
            a.y = j * 2.0 / (height - 1) - 1.0;
            a.z = 0.5;
            b.r = i * 1.0 / width;
            b.g = 0.0;
            b.b = j * 1.0 / height;
            vertices[i + j * width] = a;
            // colors[i + j * width] = b;
            Point ppp;
            ppp.r = i * 1.0 / (width-1); ppp.g = j * 1.0 / (height-1); ppp.b = 0;
            colors[i + j * width] = ppp;
        }
    }

    // indices.resize(width * height * 6);
    indices.resize(0);

    for(int j = 0; j < height - 2; ++j){
        for(int i = 0; i < width - 2; ++i){
            int t1v1 = (i) + (j) * width;
            int t1v2 = (i) + (j + 1) * width;
            int t1v3 = (i + 1) + (j) * width;

            int t2v1 = (i + 1) + (j) * width;
            int t2v2 = (i) + (j + 1) * width;
            int t2v3 = (i + 1) + (j + 1) * width;

            indices.push_back(t1v1);
            indices.push_back(t1v2);
            indices.push_back(t1v3);

            indices.push_back(t2v1);
            indices.push_back(t2v2);
            indices.push_back(t2v3);
        }
    }

    buff.init();
    buff.atribut(3);
    buff.atribut_color(3);
}



void GLEngine::newFrame()
{
    static bool first_frame = true;
    if(m_run_raytracer){
        rayEngine.getFrame(colors);
    }

    if(first_frame){
        first_frame = false;
        m_run_raytracer = false;
    }
    // timRedner.start();
    // buff.copyVboColors();
}



void GLEngine::render()
{
    program.use();

    buff.bindVertexArray();

    // glActiveTexture(GL_TEXTURE0);
    // glBindTexture(GL_TEXTURE_2D, imageTexture);

    if(render_points)
        glDrawArrays(GL_POINTS, 0, rayEngine.m_width * rayEngine.m_height);
    else
        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, NULL);

    // timRedner.stop();
}










void GLEngine::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_A){
        rayEngine.cam.start.x -= 0.2;
    }
    if (key == GLFW_KEY_D){
        rayEngine.cam.start.x += 0.2;
    }
    if (key == GLFW_KEY_W){
        rayEngine.cam.start.z += 0.2;
    }
    if (key == GLFW_KEY_S){
        rayEngine.cam.start.z -= 0.2;
    }
    if (key == GLFW_KEY_E){
        rayEngine.cam.start.y += 0.2;
    }
    if (key == GLFW_KEY_Q){
        rayEngine.cam.start.y -= 0.2;
    }
    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS){
        m_run_raytracer = !m_run_raytracer;
    }
}

