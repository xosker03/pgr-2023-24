#include "register.h"

namespace {
   std::vector<int (*)()> & funs () {
      static std::vector<int (*)()> funs_ {};
      return funs_;
   }
}

void register_cpp (int (*p)()) {
   funs ().push_back (p); // well, push_back(std::move (name)) would be more efficient
}

int call_cpps () {
   int ret = 0;
    for (auto && fun : funs()) {
       //std::cout << "Calling unit: " << fun << "\n";
       ret += fun();
   }
   return ret;
}


