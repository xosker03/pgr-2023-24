#include "Raytracer.h"

#include "string.h"

#include <thread>

#include "zaklad/zaklad.h"



Raytracer::Raytracer(bool async, int width, int height)
{
	lateInit(async, width, height);
}

Raytracer::Raytracer()
{

}

Raytracer::~Raytracer()
{
	
}


void Raytracer::lateInit(bool async, int width, int height)
{
    m_async = async;
    m_width = width;
    m_height = height;
    m_fov = 1.0;

    m_points.resize(width * height);

    if(async){
        new std::thread(&Raytracer::run, this);
    }

    IMPL::init(width, height, (IMPL::color *) m_points.data());

    cam.start = floatV(4, 0, 1.2);
    cam.p = floatV(-0.25,0,0);
}


void Raytracer::getFrame(std::vector<struct Point> & target)
{
    if(!m_async){
        genFrame();
    }
    // for(int i = 0; i < m_points.size(); ++i){
    //     target[i].r = m_points[i].r;
    //     target[i].g = m_points[i].g;
    //     target[i].b = m_points[i].b;
    // }
    // tim.start();
    // for(auto & a : target){
    //     float c = Zaklad::fastRndf();
    //     // printf("%f\n", c);
    //     a.r = c;
    //     a.g = c;
    //     a.b = c;
    // }
    // tim.stop();
    // memcpy(target.data(), m_points.data(), m_points.size() * sizeof(struct Point));
}


void Raytracer::genFrame()
{
    // for(auto & a : m_points){
    //     a.r = ((double) rand() / (RAND_MAX));
    //     a.g = ((double) rand() / (RAND_MAX));
    //     a.b = ((double) rand() / (RAND_MAX));
    // }
    // for(auto & a : m_points){
    //     a.r = ((float) getRndChar() / (255.0f));
    //     a.g = ((float) getRndChar() / (255.0f));
    //     a.b = ((float) getRndChar() / (255.0f));
    // }

    tim.start();
    cam.fov = m_fov;
    IMPL::rayCast(m_width, m_height, cam, m_sett);
    tim.stop();
}


void Raytracer::run()
{
    while(1){
        genFrame();
    }
}

