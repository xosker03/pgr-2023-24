#ifndef ANCHOR_H
#define ANCHOR_H

#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <omp.h>

#include <vector>

#include "../Particle.h"
#include "../floatV.h"

class Anchor : public Particle
{
public:
    Anchor(floatV pos, floatV speed, float radius = 1.0e-3, uint64_t type = kulicka_e);
    Anchor(const Anchor& other);
    ~Anchor() override;

    void euler_step(float dt, floatV gravity, floatV ext_accell) override;
    void euler_finalize(float dt) override;

    floatV m_pos_anchor;
};











#endif // PARTICLE_H
