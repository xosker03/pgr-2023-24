#include "Anchor.h"

#include <limits>


Anchor::Anchor(floatV pos, floatV speed, float radius, uint64_t type) : Particle(pos, speed, radius, type)
{
    m_pos_anchor = pos;
    m_mass = std::numeric_limits<float>::infinity();
    m_inverse_mass = 0;
    m_velocity = floatV();
    m_collision_skip = true;
    m_hidden = true;
}

Anchor::Anchor(const Anchor& other) : Particle(other)
{
    m_pos_anchor = other.m_pos_anchor;
}

Anchor::~Anchor()
{

}

void Anchor::euler_step(float dt, floatV gravity, floatV ext_accell)
{
    m_pos = m_pos_anchor;
    m_velocity = floatV();
}


void Anchor::euler_finalize(float dt)
{
    m_pos = m_pos_anchor;
    m_velocity = floatV();
}




#include "../register.h"
static int self_test(){
    printc(bc.YELLOW, "Self Test: Anchor\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(self_test);

