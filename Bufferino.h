#ifndef BUFFERINO_H
#define BUFFERINO_H

#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>


template<class T, class C>
class Buffik {
public:
    Buffik(std::vector<T> * ver, std::vector<C> * col, std::vector<unsigned int> * ind = NULL){
        lateConstruct(ver, col, ind);
    }
    Buffik(){

    }
    ~Buffik(){

    }

    void lateConstruct(std::vector<T> * ver, std::vector<C> * col, std::vector<unsigned int> * ind = NULL){
        vertices = ver;
        colors = col;
        indices = ind;
        sizeofT = sizeof(T);
        sizeofC = sizeof(C);
    }

    void init(){
        if(!vertices){
            throw 1;
        }

        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glGenBuffers(2, vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);

        glGenBuffers(1, &ebo);

        copyVboData();

        if(indices){
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices->size(), indices->data(), GL_STATIC_DRAW);
        }
    }

    void atribut(GLuint atSize){
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        glEnableVertexAttribArray(atIdx);
        glVertexAttribPointer(atIdx, atSize, GL_FLOAT, GL_FALSE, sizeof(T), (GLuint*) atOffset);

        ++atIdx;
        atOffset += atSize * sizeof(float);
    }
    void atribut_color(GLuint atSize){
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glEnableVertexAttribArray(atIdx);
        glVertexAttribPointer(atIdx, atSize, GL_FLOAT, GL_FALSE, sizeof(T), (GLuint*) atOffset);

        ++atIdx;
        atOffset += atSize * sizeof(float);
    }

    void copyVboData(){
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        glBufferData(GL_ARRAY_BUFFER, vertices->size() * sizeofT, vertices->data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, colors->size() * sizeofC, colors->data(), GL_DYNAMIC_DRAW);
    }

    void copyVboColors(){
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, colors->size() * sizeofC, colors->data(), GL_DYNAMIC_DRAW);
    }

    void bindVertexArray(){
        glBindVertexArray(vao);
    }

    GLuint vao = 0;
    GLuint vbo[2];
    GLuint ebo;

    uint8_t * atOffset = 0;
    GLuint atIdx = 0;

    unsigned int sizeofT;
    unsigned int sizeofC;

    std::vector<T> * vertices = NULL;
    std::vector<C> * colors = NULL;
    std::vector<unsigned int> * indices = NULL;
};


#endif
