#include "Shrekder.h"


Shrekder::Shrekder(std::string name)
{
	m_programName = name;
}

Shrekder::~Shrekder()
{
	
}

std::string Shrekder::read_file(const char* file_path){
    std::ifstream file(file_path);
    if (!file) {
        std::cerr << "Nepodařilo se otevřít soubor: " << file_path << std::endl;
        throw "readfile";
        return "";
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    return buffer.str();
}

void Shrekder::compile(GLenum type, std::string name, std::string file){
    if(file == ""){
        file = name + ".glsl";
    }
    file = "shaders/" + file;
    std::string data = read_file(file.c_str());
    const char* src = data.c_str();
    unsigned int shader;
    shader = glCreateShader(type);
    glShaderSource(shader, 1, &src, NULL);
    glCompileShader(shader);
    int  success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cout << m_programName << "::" << name << "::COMPILATION_FAILED\n";
        std::cout << infoLog << std::endl;
        throw 1;
    }
    m_shader.push_back(shader);
}

void Shrekder::compile_vertex(){
    compile(GL_VERTEX_SHADER, "Vertex", "vertex.glsl");
}
void Shrekder::compile_fragment(){
    compile(GL_FRAGMENT_SHADER, "Fragment", "fragment.glsl");
}

void Shrekder::link(){
    m_mainProgram = glCreateProgram();
    for(auto & a : m_shader){
        glAttachShader(m_mainProgram, a);
    }
    int  success;
    char infoLog[512];
    glLinkProgram(m_mainProgram);
    glGetProgramiv(m_mainProgram, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(m_mainProgram, 512, NULL, infoLog);
        std::cout << m_programName << "::LINK_FAILED\n";
        std::cout << infoLog << std::endl;
        throw "link failed";
    }
    for(auto & a : m_shader){
        glDeleteShader(a);
    }
    m_shader.resize(0);
    linked = true;
}

void Shrekder::use(){
    if(!linked){
        std::cerr << m_programName << "::NOT LINKED" << std::endl;
        throw "not linked";
    }
    glUseProgram(m_mainProgram);
}






// //GEOMETRY SHADER
// auto geometryShader_src = read_file("geometry.glsl");
// program.compile(GL_GEOMETRY_SHADER, geometryShader_src.c_str(), "Geometry");
//
// //VERTEX SHADER
// auto vertexShader_src = read_file("vertex.glsl");
// program.compile_vertex(vertexShader_src.c_str());
//
// //FRAGMENT SHADER
// auto fragmentShader_src = read_file("fragment.glsl");
// program.compile_fragment(fragmentShader_src.c_str());
//
// //FINAL SHADER
// program.link();
// program.use();

