#include "RayGPU.h"

#include <GL/glew.h>

#include "../Shrekder.h"

#include "../GLEngine.h"

#include "../Object/bunny_mesh.h"

#include "../Simulation.h"

#include <thread>

namespace RayGPU {

std::vector<object> obaly;
std::vector<object> objekty;
static ObjectStorage storage;
// static std::vector<ray> paprsky;
static color * vystup;

static std::vector<debug> debuginfo;

Simulation *SIM;
std::thread *th_gl;

GLuint objektBuff;
GLuint colorBuff;
GLuint debugBuff;
GLuint camBuff;

GLuint width_Uniform;
GLuint height_Uniform;
GLuint ratio_Uniform;
GLuint objectCount_Uniform;
GLuint realSeed_Uniform;
GLuint WORK_GROUP_CYCLES_Uniform;
GLuint RAY_BOUNCES_Uniform;
GLuint AVERAGE_RENDER_Uniform;
GLuint OBJ_OFFSET_Uniform;

GLuint camera_start_Uniform;
GLuint camera_p_Uniform;

Shrekder comprogram("RayProgram");


void rayCast(int width, int height, camera cam, Settings sett){
    float ratio = height * 1.0 / width;

    float alfa = cam.p.x * M_PI;
    cam.rotaceX = floatM3(cosf(alfa), 0, sinf(alfa),
                    0, 1, 0,
                    -sinf(alfa), 0, cosf(alfa));

    float beta = cam.p.y * M_PI;
    cam.rotaceY = floatM3(1, 0, 0,
                    0, cosf(beta), -sinf(beta),
                    0, sinf(beta), cosf(beta));

    float gama = cam.p.z * M_PI;
    cam.rotaceZ = floatM3(cosf(gama), -sinf(gama), 0,
                    sinf(gama), cosf(gama), 0,
                    0, 0, 1);

    comprogram.use();

    glProgramUniform1i(comprogram.m_mainProgram, width_Uniform, width);
    glProgramUniform1i(comprogram.m_mainProgram, height_Uniform, height);
    glProgramUniform1f(comprogram.m_mainProgram, ratio_Uniform, ratio);
    glProgramUniform1i(comprogram.m_mainProgram, objectCount_Uniform, storage.pocet_obalu);
    glProgramUniform1ui(comprogram.m_mainProgram, realSeed_Uniform, Zaklad::rnd32());
    glProgramUniform1ui(comprogram.m_mainProgram, WORK_GROUP_CYCLES_Uniform, sett.work_group_cycles);
    glProgramUniform1ui(comprogram.m_mainProgram, RAY_BOUNCES_Uniform, sett.ray_bounces);
    glProgramUniform1ui(comprogram.m_mainProgram, AVERAGE_RENDER_Uniform, sett.average_render);
    glProgramUniform1i(comprogram.m_mainProgram, OBJ_OFFSET_Uniform,storage.pocet_obalu);

    // glNamedBufferSubData(objektBuff, 0, objekty.size() * sizeof(struct object), objekty.data());
    glNamedBufferSubData(camBuff, 0, sizeof(struct camera), &cam);

    ENGINE->num_objs = storage.size;
    storage.cpy2gpu();

    if(sett.rrtracing)
        glDispatchCompute(width / 32 + 1, height / 32 + 1, 1);
    else
        glDispatchCompute(width, height, 1);
    // glMemoryBarrier(GL_ALL_BARRIER_BITS);
    // glGetNamedBufferSubData(colorBuff, 0, width * height * sizeof(glm::vec3), vystup);
    // glGetNamedBufferSubData(debugBuff, 0, width * height * sizeof(debug), debuginfo.data());

    // debuginfo[256/2 + 144/2 * 256 - 2].print();
    // debuginfo[256/2 + 144/2 * 256 - 1].print();
    // debuginfo[256/2 + 144/2 * 256].print();
    // debuginfo[256/2 + 144/2 * 256 + 1].print();
    // debuginfo[256/2 + 144/2 * 256 + 2].print();
    // for(auto & a : debuginfo){
    //     a.print();
    // }
    // printf("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n");
    // throw 1;

    if(ENGINE->m_simulate){
        static float poloha = 1;
        object o = storage.get(0);
        o.stred.x = sinf(poloha * 0.5) * 20.0;
        o.stred.y = (cosf(poloha * 0.5)) * 4.0 + 2.0;
        o.stred.z = cosf(poloha * 0.5) * 20.0;

        if(sett.party){
            o.barva.x = sinf(poloha * 3.0) * 100.0;
            o.barva.y = sinf(poloha * 1.5) * 100.0;
        } else {
            o.barva = floatV(100,100,100);
        }

        poloha += 2e-2;
        storage.set(0, o);

        SIM->m_sim_pause = false;
        // SIM->step();
    } else {
        SIM->m_sim_pause = true;
    }
}



void init(int width, int height, color * _vystup)
{
    vystup = _vystup;
    debuginfo.resize(width * height);

    //bílé slunce
    obaly.push_back(genBall(true, floatV(100, 100, 100), 0.0, floatV(0, 0, -10), 2.0));

    //bílá koule malá
    obaly.push_back(genBall(false, floatV(1, 1, 1), 0.0, floatV(1, 1.3, 6), 1.0));

    //žlutá koule
    obaly.push_back(genBall(false, floatV(1, 1, 0), 1.0, floatV(1, -0.5, 7), 0.8));

    //červeno šedá koule
    obaly.push_back(genBall(false, floatV(0.9, 0.01, 0.01), 0.5, floatV(0, 0, 4), 1.0));

    // //modré slunce
    // obaly.push_back(genBall(true, floatV(0, 0, 1), 0.0, floatV(0, 0, 14), 1.0));

    // //tyrkysové slunce
    // obaly.push_back(genBall(true, floatV(0, 1, 1), 0.0, floatV(10, 0, 0), 1.0));

    //zmemě
    // obaly.push_back(genBall(false, floatV(0, 0.6, 0), 1.0, floatV(0, -1002, 0), 1000.0));

    //trojúhelník
    obaly.push_back(genTriangle(false, floatV(0.1, 0.1, 1), 0.5, floatV(-5,-1,1), floatV(-4,-1,7), floatV(-4,4,4)));

    float cdst = 25;
    //levá
    obaly.push_back(genTriangle(false, floatV(0.3, 0, 0), 1, floatV(-cdst,-cdst,-cdst), floatV(-cdst,-cdst,cdst), floatV(-cdst,cdst,-cdst)));
    obaly.push_back(genTriangle(false, floatV(0.3, 0, 0), 1, floatV(-cdst,cdst,cdst), floatV(-cdst,cdst,-cdst), floatV(-cdst,-cdst,cdst)));

    //zadní
    obaly.push_back(genTriangle(false, floatV(0, 0.3, 0), 1, floatV(-cdst,-cdst,-cdst), floatV(-cdst,cdst,-cdst), floatV(cdst,-cdst,-cdst)));
    obaly.push_back(genTriangle(false, floatV(0, 0.3, 0), 1, floatV(cdst,-cdst,-cdst), floatV(-cdst,cdst,-cdst), floatV(cdst,cdst,-cdst)));

    //pravá
    obaly.push_back(genTriangle(false, floatV(0, 0, 0.3), 1, floatV(cdst,-cdst,-cdst), floatV(cdst,cdst,-cdst), floatV(cdst,-cdst,cdst)));
    obaly.push_back(genTriangle(false, floatV(0, 0, 0.3), 1, floatV(cdst,cdst,cdst), floatV(cdst,-cdst,cdst), floatV(cdst,cdst,-cdst)));

    //přední
    obaly.push_back(genTriangle(false, floatV(0.3, 0.3, 0.1), 0, floatV(-cdst,-cdst,cdst), floatV(cdst,-cdst,cdst), floatV(-cdst,cdst,cdst)));
    obaly.push_back(genTriangle(false, floatV(0.3, 0.3, 0.1), 0, floatV(cdst,-cdst,cdst), floatV(cdst,cdst,cdst), floatV(-cdst,cdst,cdst)));

    //strop
    obaly.push_back(genTriangle(false, floatV(0.3, 0.3, 0.3), 1, floatV(-cdst,cdst,-cdst), floatV(-cdst,cdst,cdst), floatV(cdst,cdst,-cdst)));
    obaly.push_back(genTriangle(false, floatV(0.3, 0.3, 0.3), 1, floatV(cdst,cdst,cdst), floatV(cdst,cdst,-cdst), floatV(-cdst,cdst,cdst)));

    //spodek
    obaly.push_back(genTriangle(false, floatV(0, 0.6, 0), 1, floatV(-cdst,-2,-cdst), floatV(cdst,-2,-cdst), floatV(-cdst,-2,cdst)));
    obaly.push_back(genTriangle(false, floatV(0, 0.6, 0), 1, floatV(cdst,-2,cdst), floatV(-cdst,-2,cdst), floatV(cdst,-2,-cdst)));


    spawnBunny(floatV(3,-1.5,5), 1.5, floatV(0, 0.1, 0.5), 1, false);
    spawnBunny(floatV(3,0,8), 2, floatV(0.5, 0, 0), 1);

    SIM = new Simulation(false, 5);
    th_gl = new std::thread(&Simulation::run, SIM);

    comprogram.compile(GL_COMPUTE_SHADER, "RayShader", "../shaders/compute_ray.glsl");
    // comprogram.compile(GL_COMPUTE_SHADER, "RayShader", "../shaders/compute_path.glsl");
    comprogram.link();
    comprogram.use();

    storage.reserve();

    for(auto &a : obaly){
        storage.push_back(a);
    }
    storage.pocet_obalu = obaly.size();
    for(auto &a : objekty){
        storage.push_back(a);
    }
    storage.pocet_objektu = objekty.size();

    // glCreateBuffers(1, &objektBuff);
    glCreateBuffers(1, &colorBuff);
    glCreateBuffers(1, &debugBuff);
    glCreateBuffers(1, &camBuff);
    // glNamedBufferStorage(objektBuff, objekty.size() * sizeof(struct object), objekty.data(), GL_DYNAMIC_STORAGE_BIT);
    glNamedBufferStorage(colorBuff, width * height * sizeof(struct color), vystup, GL_DYNAMIC_STORAGE_BIT);
    glNamedBufferStorage(debugBuff, debuginfo.size() * sizeof(struct debug), debuginfo.data(), GL_DYNAMIC_STORAGE_BIT);
    glNamedBufferStorage(camBuff, sizeof(struct camera), vystup, GL_DYNAMIC_STORAGE_BIT);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, objektBuff);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, colorBuff);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, debugBuff);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, camBuff);

    storage.init();

    width_Uniform = glGetUniformLocation(comprogram.m_mainProgram,"width");
    height_Uniform  = glGetUniformLocation(comprogram.m_mainProgram,"height");
    ratio_Uniform  = glGetUniformLocation(comprogram.m_mainProgram,"ratio");
    objectCount_Uniform = glGetUniformLocation(comprogram.m_mainProgram,"objectCount");
    realSeed_Uniform = glGetUniformLocation(comprogram.m_mainProgram,"realSeed");
    WORK_GROUP_CYCLES_Uniform = glGetUniformLocation(comprogram.m_mainProgram,"WORK_GROUP_CYCLES");
    RAY_BOUNCES_Uniform = glGetUniformLocation(comprogram.m_mainProgram,"RAY_BOUNCES");
    AVERAGE_RENDER_Uniform = glGetUniformLocation(comprogram.m_mainProgram,"AVERAGE_RENDER");
    OBJ_OFFSET_Uniform = glGetUniformLocation(comprogram.m_mainProgram,"OBJ_OFFSET");

    glGenTextures(1, &ENGINE->imageTexture);
    glBindTexture(GL_TEXTURE_2D, ENGINE->imageTexture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
    glBindImageTexture(0, ENGINE->imageTexture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
}



void recompile(bool tracer)
{
    std::string prog = "";
    if(tracer)
        prog = "../shaders/compute_ray.glsl";
    else
        prog = "../shaders/compute_path.glsl";
    RayGPU::comprogram.compile(GL_COMPUTE_SHADER, "RayShader", prog);
    RayGPU::comprogram.link();
}


struct object genObj(bool svetlo, floatV barva, float matnost, uint extra_typ)
{
    struct object o;
    if(svetlo)
        o.typ = svetlo_e;
    o.typ |= extra_typ;
    o.barva = barva;
    o.matnost = matnost;
    return o;
}

struct object genBall(bool svetlo, floatV barva, float matnost, floatV stred, float r, uint extra_typ)
{
    struct object o = genObj(svetlo, barva, matnost, extra_typ);
    o.typ |= koule_e;
    o.stred = stred;
    o.r = r;
    return o;
}

struct object genTriangle(bool svetlo, floatV barva, float matnost, floatV A1, floatV A2, floatV A3, uint extra_typ)
{
    struct object o = genObj(svetlo, barva, matnost, extra_typ);
    o.typ |= trojuhelnik_e;
    o.bod[0] = A1;
    o.bod[1] = A2;
    o.bod[2] = A3;
    o.normie = ((A2 - A1) ^ (A3 - A1)).norm();
    o.stred = (A1 + A2 + A3) / 3;
    // o.r = fmax(fmax((A1 - o.stred).len(), (A2 - o.stred).len()), (A3 - o.stred).len());
    o.r = -(o.normie.x * A1.x + o.normie.y * A1.y + o.normie.z * A1.z);
    return o;
}




struct object ObjectStorage::get(int idx)
{
    if(idx >= size)
        throw 1;
    struct object o;
    o.typ = typ[idx];
    o.bod[0] = bod0[idx];
    o.bod[1] = bod1[idx];
    o.bod[2] = bod2[idx];
    o.normie = normie[idx];
    o.stred = stred[idx];
    o.r = r[idx];
    o.barva = barva[idx];
    o.matnost = matnost[idx];
    return o;
}

void ObjectStorage::set(int idx, object o)
{
    if(idx >= size)
        throw 1;
    typ[idx] = o.typ;
    bod0[idx] = o.bod[0];
    bod1[idx] = o.bod[1];
    bod2[idx] = o.bod[2];
    normie[idx] = o.normie;
    stred[idx] = o.stred;
    r[idx] = o.r;
    barva[idx] = o.barva;
    matnost[idx] = o.matnost;
}

void ObjectStorage::push_back(object o)
{
    size++;
    typ.push_back(o.typ);
    bod0.push_back(o.bod[0]);
    bod1.push_back(o.bod[1]);
    bod2.push_back(o.bod[2]);
    normie.push_back(o.normie);
    stred.push_back(o.stred);
    r.push_back(o.r);
    barva.push_back(o.barva);
    matnost.push_back(o.matnost);
    if(size >= rezerva){
        printc(bc.CRITIC, "ObjectStorage: Došla rezerva\n");
    }
    setReferences(size - 1, o.bod_ref[0], o.bod_ref[1], o.bod_ref[2], o.stred_ref);
}

void ObjectStorage::reserve()
{
    typ.reserve(rezerva);
    bod0.reserve(rezerva);
    bod1.reserve(rezerva);
    bod2.reserve(rezerva);
    normie.reserve(rezerva);
    stred.reserve(rezerva);
    r.reserve(rezerva);
    barva.reserve(rezerva);
    matnost.reserve(rezerva);
}


void ObjectStorage::init()
{
    if(size == 0)
        throw 1;
    gpuBuffs.resize(size);

    init_datas();

    for(int i = 0; i < storages; ++i){
        printblue("Init Storage: %d:%d | %d * %u, %p\n", i, i + layout_offset, size, sizeofs[i], datas[i]);
        glCreateBuffers(1, &gpuBuffs[i]);
        glNamedBufferStorage(gpuBuffs[i], size * sizeofs[i], datas[i], GL_DYNAMIC_STORAGE_BIT);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, layout_offset + i, gpuBuffs[i]);
    }
    printblue("obaly: %d, objekty %d, celkem %d:%d\n", pocet_obalu, pocet_objektu, size, pocet_obalu + pocet_objektu);
}

void ObjectStorage::init_datas()
{
    datas[0] = typ.data();
    datas[1] = bod0.data();
    datas[2] = bod1.data();
    datas[3] = bod2.data();
    datas[4] = normie.data();
    datas[5] = stred.data();
    datas[6] = r.data();
    datas[7] = barva.data();
    datas[8] = matnost.data();
}


void ObjectStorage::cpy2gpu()
{
    init_datas();
    for(int i = 0; i < storages; ++i){
        glNamedBufferSubData(gpuBuffs[i], 0, size * sizeofs[i], datas[i]);
    }
}

void ObjectStorage::setReferences(int idx, std::vector<floatV *> * b0, std::vector<floatV *> * b1, std::vector<floatV *> * b2, std::vector<floatV *> * st)
{
    if(idx >= size)
        throw 1;
    if(b0){
        b0->push_back(&bod0[idx]);
        // printf("*ref -> ref %p -> %p\n", b0, &bod0[idx]);
    }
    if(b1){
        b1->push_back(&bod1[idx]);
        // printf("*ref -> ref %p -> %p\n", b1, &bod1[idx]);
    }
    if(b2){
        b2->push_back(&bod2[idx]);
        // printf("*ref -> ref %p -> %p\n", b2, &bod2[idx]);
    }
    if(st){
        st->push_back(&stred[idx]);
        // printf("*ref -> ref %p -> %p\n", b2, &bod2[idx]);
    }

}


void spawnBunny(floatV pos, float scale, floatV barva, float matnost, bool svetlo)
{
    int size_verts = sizeof(verts) / sizeof(float);
    int size_tetSurfaceTriIds = sizeof(tetSurfaceTriIds) / sizeof(int);
    floatV stred;
    float polomer = 0;
    int obj_count = 0;
    int obj_offset = objekty.size();
    for(int i = 0; i < (size_tetSurfaceTriIds - 3); i += 3){
        int body[3] = {tetSurfaceTriIds[i], tetSurfaceTriIds[i+1], tetSurfaceTriIds[i+2]};
        floatV np0 = pos + floatV(verts[body[0]*3], verts[body[0]*3+1], verts[body[0]*3+2]) * scale;
        floatV np1 = pos + floatV(verts[body[1]*3], verts[body[1]*3+1], verts[body[1]*3+2]) * scale;
        floatV np2 = pos + floatV(verts[body[2]*3], verts[body[2]*3+1], verts[body[2]*3+2]) * scale;
        stred += floatV(verts[body[0]*3], verts[body[0]*3+1], verts[body[0]*3+2])
                + floatV(verts[body[1]*3], verts[body[1]*3+1], verts[body[1]*3+2])
                + floatV(verts[body[2]*3], verts[body[2]*3+1], verts[body[2]*3+2]);
        obj_count++;
        objekty.push_back(genTriangle(svetlo, barva, matnost, np0, np2, np1));
    }
    stred /= obj_count;
    for(int i = obj_offset; i < obj_offset + obj_count; ++i){
        int body[3] = {tetSurfaceTriIds[i], tetSurfaceTriIds[i+1], tetSurfaceTriIds[i+2]};
        floatV np0 = floatV(verts[body[0]*3], verts[body[0]*3+1], verts[body[0]*3+2]);
        floatV np1 = floatV(verts[body[1]*3], verts[body[1]*3+1], verts[body[1]*3+2]);
        floatV np2 = floatV(verts[body[2]*3], verts[body[2]*3+1], verts[body[2]*3+2]);
        polomer = fmax(polomer, (np0 - stred).len());
        polomer = fmax(polomer, (np1 - stred).len());
        polomer = fmax(polomer, (np2 - stred).len());
    }
    polomer *= scale * 0.5;
    stred += pos;
    stred += floatV(0, -0.25 * scale, 0);

    object ob = genObj(false, barva, matnost, obal_e);
    ob.stred = stred;
    ob.r = polomer;
    ob.normie = floatV(obj_offset, obj_count, 0);
    obaly.push_back(ob);
}




}
