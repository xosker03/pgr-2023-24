#ifndef RayImpl_H
#define RayImpl_H

#include <stdint.h>
#include <math.h>

#include <vector>
#include <limits>

#include "../floatV.h"
#include "../floatM.h"

namespace RayTr {

    enum objType : uint32_t {
        nic_e = 0,
        koule_e = (1 << 0),
        trojuhelnik_e = (1 << 1),
        svetlo_e = (1 << 2)
    };

    struct ray;
    struct object;
    struct camera;
    struct color;



    // extern std::vector<object> objekty;
    // extern std::vector<ray> paprsky;
    // extern color * vystup;


    struct color : floatV{
        inline color(float _r, float _g, float _b){
            x = _r; y = _g; z = _b;
        }
        inline color() : floatV(1,1,1){
        }
        inline color(floatV &r) : floatV(r){
        }
        inline color(floatV r) : floatV(r){
        }

        inline color mix(color svetlo){
            color ret = svetlo;
            ret.x *= x;
            ret.y *= y;
            ret.z *= z;
            // ret.x = ret.x * 0.95 + svetlo.x * 0.05;
            // ret.y = ret.y * 0.95 + svetlo.y * 0.05;
            // ret.z = ret.z * 0.95 + svetlo.z * 0.05;
            ret = (ret * 0.90) + (svetlo * 0.10);
            return ret;
        }

        inline color& operator=(const floatV& source) {
            x = source.x;
            y = source.y;
            z = source.z;
            return *this;
        }
    };

    struct ray {
        inline ray(floatV start, floatV end){
            a = start;
            p = (end - start).norm();
        }

        inline floatV getPoint(float t){
            return a + t * p;
        }

        inline void shift(float s){
            a += s * p;
        }

        inline color cast(int budget, object * skip = NULL, bool draw = false);
        // inline color spawn();

        floatV a;
        floatV p;
    };



    struct object {
        inline float prunik(ray paprsek);
        inline floatV normala(floatV x);

        uint32_t id = 0;
        uint32_t typ = nic_e;
        floatV bod[3];
        float r = 0;
        color barva;
        float matnost = 0.0;
        float pruhlednost = 0;
    };



    struct camera {
        floatV start;
        floatV p;
        float fov;
    };


    void init(int width, int height, color * _vystup);
    void rayCast(int width, int height, camera cam);

    floatV randomDirection(uint32_t state);

}
#endif

