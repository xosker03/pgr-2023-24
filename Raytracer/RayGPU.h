#ifndef RayGPU_H
#define RayGPU_H

#include <stdint.h>
#include <math.h>

#include <vector>
#include <limits>

#include "../floatV.h"
#include "../floatM.h"

#include <glm/glm.hpp>

struct Settings{
    int work_group_cycles = 1;
    int ray_bounces = 3;
    int average_render = 0;
    int average_render_max = 0;
    bool rrtracing = true;
    bool party = false;
};


namespace RayGPU {

    enum objType : uint32_t {
        nic_e = 0,
        koule_e = (1 << 0),
        trojuhelnik_e = (1 << 1),
        svetlo_e = (1 << 2),
        obal_e = (1 << 3)
    };

    struct ray;
    struct object;
    struct camera;
    // using color = glm::vec3;
    struct color {
        float r,g,b;
    };

    // struct ray {
    //     glm::vec3 A;
    //     glm::vec3 p;
    // };

    struct object {
        uint typ = nic_e;
        floatV bod[3];
        floatV normie; //obal: obj_offset, obj_count
        floatV stred;
        float r = 0;
        floatV barva;
        float matnost = 0.0;
        // float pruhlednost = 0;
        std::vector<floatV *> * bod_ref[3] = {NULL, NULL, NULL};
        std::vector<floatV *> * stred_ref = NULL;
    };

    struct ObjectStorage{
        struct object get(int idx);
        void set(int idx, object o);
        void push_back(object o);

        void reserve();
        void init();
        void init_datas();
        void cpy2gpu();
        void setReferences(int idx, std::vector<floatV *> * b0, std::vector<floatV *> * b1, std::vector<floatV *> * b2, std::vector<floatV *> * st = NULL);

        std::vector<uint> typ;
        std::vector<floatV> bod0;
        std::vector<floatV> bod1;
        std::vector<floatV> bod2;
        std::vector<floatV> normie;
        std::vector<floatV> stred;
        std::vector<float> r;
        std::vector<floatV> barva;
        std::vector<float> matnost;

        int size = 0;
        size_t sizeofs[9] = {sizeof(uint), sizeof(floatV), sizeof(floatV), sizeof(floatV), sizeof(floatV), sizeof(floatV), sizeof(float), sizeof(floatV), sizeof(float)};
        void * datas[9];

        std::vector<unsigned int> gpuBuffs;
        int storages = 9;
        int layout_offset = 4;

        int pocet_obalu;
        int pocet_objektu;

        int rezerva = 3000;
    };

    struct camera {
        floatV start;
        floatV p;
        float fov;
        floatM3 rotaceX;
        floatM3 rotaceY;
        floatM3 rotaceZ;
    };

    struct debug{
        void print(){
            printf("%u %u %u | %d %d %d | %f %f %f %f\n", u0, u1, u2, d0, d1, d2, f0, f1, f2, f3);
        }

        uint u0 = 0;
        uint u1 = 0;
        uint u2 = 0;
        int d0 = 0;
        int d1 = 0;
        int d2 = 0;
        float f0 = 0;
        float f1 = 0;
        float f2 = 0;
        float f3 = 0;
    };

    extern std::vector<object> obaly;
    extern std::vector<object> objekty;

    void init(int width, int height, color * _vystup);
    void rayCast(int width, int height, camera cam, Settings sett);

    void recompile(bool tracer);

    struct object genObj(bool svetlo, floatV barva, float matnost, uint extra_typ = 0);
    struct object genBall(bool svetlo, floatV barva, float matnost, floatV stred, float r, uint extra_typ = 0);
    struct object genTriangle(bool svetlo, floatV barva, float matnost, floatV A1, floatV A2, floatV A3, uint extra_typ = 0);

    void spawnBunny(floatV pos, float scale, floatV barva, float matnost, bool svetlo = false);
}
#endif

