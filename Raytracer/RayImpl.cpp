#include "RayImpl.h"

#include "../Dengine.h"

namespace RayTr {


static std::vector<object> objekty;
static std::vector<ray> paprsky;
static color * vystup;


inline float object::prunik(ray paprsek){
    if(typ & koule_e){
        floatV & a = paprsek.a;
        floatV & p = paprsek.p;
        floatV & c = bod[0];
        float aa = p & p;
        float bb = 2 * (p & (a - c));
        float cc = ((a - c) & (a - c)) - r*r;
        float D = bb * bb - 4*aa*cc;
        if(D >= 0){
            float sD = sqrt(D);
            float x1 = (-bb + sD) / (2 * aa);
            float x2 = (-bb - sD) / (2 * aa);
            return fmin(x1, x2);
        }
    }
    return NAN;
}


inline floatV object::normala(floatV x)
{
    if(typ & koule_e){
        floatV & s = bod[0];
        floatV n = x - s;
        return n.norm();
    }
    return floatV();
}



inline color ray::cast(int budget, object * skip, bool draw){
    if(budget == 0)
        return color(0, 0, 0);

    object * nejblizsi = NULL;
    float vzdalenost = std::numeric_limits<float>::max();

    for(auto & a : objekty){
        if(&a == skip)
            continue;
        float p = a.prunik(*this);
        if(!isnan(p)){
            if(p > 0 && p < vzdalenost){
                nejblizsi = &a;
                vzdalenost = p;
            }
        }
    }

    // Dengine::drawLine(this->a, this->a + this->p*4, floatV(1,1,1));

    if(nejblizsi){
        if(nejblizsi->id == 4)
            draw = true;
        if(draw)
            Dengine::drawLine(this->a, getPoint(vzdalenost), floatV(abs(this->a.x / 10.0), abs(this->a.y / 10.0), abs(this->a.z / 10.0)));

        if(nejblizsi->typ & svetlo_e){
            floatV X = getPoint(vzdalenost);
            floatV norm = nejblizsi->normala(X);
            float skalar = norm & p.norm();
            return nejblizsi->barva * -skalar;
        } else {
            //TODO rekurze
            floatV X = getPoint(vzdalenost);
            floatV norm = nejblizsi->normala(X);
            if(!norm.isnan()){
                floatV bod_soumernosti = X + norm * -(norm & (p * vzdalenost));
                floatV vektor_soumernosti = bod_soumernosti - a;
                floatV novy_bod = a + vektor_soumernosti + vektor_soumernosti;
                ray novacek(X, novy_bod);

                color nova_barva = nejblizsi->barva.mix(novacek.cast(budget - 1, nejblizsi, draw));
                if(nejblizsi->matnost != 0){
                    uint32_t pocet = 100;// * budget;
                    color dalsi_barva(0,0,0);
                    for(uint32_t i = 0; i < pocet; ++i){
                        floatV novy_bod = randomDirection(i);
                        ray novacek(floatV(), novy_bod);
                        if((novacek.p & norm) < 0){
                            novacek.p *= -1.0;
                        }
                        novacek.a = X;
                        if(draw) Dengine::drawLine(novacek.a, novacek.a + novacek.p * 2, floatV(abs(novacek.a.x / 10.0), abs(novacek.a.y / 10.0), abs(novacek.a.z / 10.0)));
                        dalsi_barva += novacek.cast(budget - 1, nejblizsi, draw);
                    }
                    nova_barva = nova_barva * (1.0f - nejblizsi->matnost) + ((dalsi_barva / pocet) * nejblizsi->matnost);
                }
                return nova_barva;
            } else {
                ray novacek(X, a);
                return nejblizsi->barva.mix(novacek.cast(budget - 1, nejblizsi, draw));
            }
        }
    }

    // return color(0.1, 0.1, 0.1);
    return color(0, 0, 0);
    // return color(0.5, 0.5, 0.5);
}



void rayCast(int width, int height, camera cam){
    float ratio = height * 1.0 / width;

    float alfa = cam.p.x * M_PI;
    floatM3 rotaceX(cosf(alfa), 0, sinf(alfa),
                    0, 1, 0,
                    -sinf(alfa), 0, cosf(alfa));

    float beta = cam.p.y * M_PI;
    floatM3 rotaceY(1, 0, 0,
                    0, cosf(beta), -sinf(beta),
                    0, sinf(beta), cosf(beta));

    float gama = cam.p.z * M_PI;
    floatM3 rotaceZ(cosf(gama), -sinf(gama), 0,
                    sinf(gama), cosf(gama), 0,
                    0, 0, 1);

    floatM3 rotace = rotaceX + rotaceY + rotaceZ;

    #pragma omp parallel for firstprivate(cam, vystup) collapse(2) schedule(static, 10)
    for(int j = 0; j < height; ++j){
        for(int i = 0; i < width; ++i){
            float x = i * 2.0 / (width) - 1.0;
            float y = j * 2.0 / (height) - 1.0;
            floatV smer(x * cam.fov, y * cam.fov * ratio, 1);
            // smer = smer.norm() + cam.p;
            smer = smer ^ rotaceX ^ rotaceY ^ rotaceZ;
            smer = smer.norm();
            ray pap(cam.start, cam.start + smer);

            // color barva;
            // if(j == height/2 && i == (width/2))
            //     barva = pap.cast(3, NULL, true);

            color barva = pap.cast(3);
            vystup[i + j * width] = barva;

            // if(j == 0 || j == height-1 || i == 0 || i == width-1 || (i == width/2 && j == height/2))
            //     Dengine::drawLine(pap.a, pap.a + pap.p * 4, floatV(pap.a.x / 10.0, pap.a.y / 10.0, pap.a.z / 10.0));
        }
    }

    static float poloha = 1;
    object & o = objekty.front();
    o.bod[0].x = sinf(poloha) * 20;
    o.bod[0].y = (cosf(poloha)) * 4;
    o.bod[0].z = cosf(poloha) * 20;
    poloha += 2e-2;

    for(auto & a : objekty){
        Dengine::drawSphere(a.bod[0], a.r, a.barva);
    }
}



static inline uint32_t fastRnd32(uint32_t fast_see){
    static uint32_t fast_seed = 123155689;
    fast_seed ^= (fast_seed << 13);
    fast_seed ^= (fast_seed >> 17);
    fast_seed ^= (fast_seed << 5);
    return fast_seed;
}
float inline fastRndf(uint32_t fast_seed){
    return fastRnd32(fast_seed) / ((float)UINT32_MAX + 1.0);
}

static uint32_t randomNormalDist(uint32_t state){
    //https://stackoverflow.com/a/617829
    float theta = 2 * M_PI * fastRnd32(state);
    float rho = sqrtf(-2 * logf(fastRnd32(state)));
    return rho * cosf(theta);
}
floatV randomDirection(uint32_t state){
    //https://math.stackexchange.com/a/1585996
    // floatV d(randomNormalDist(state), randomNormalDist(state), randomNormalDist(state));
    // floatV d(fastRnd32(state), fastRnd32(state), fastRnd32(state));
    floatV d(fastRndf(state)*2.0-1.0, fastRndf(state)*2.0-1.0, fastRndf(state)*2.0-1.0);
    return d.norm();
}



void init(int width, int height, color * _vystup)
{
    vystup = _vystup;

    object o;

    //bílé slunce
    o.id = 1;
    o.typ = koule_e | svetlo_e;
    o.bod[0] = floatV(0, 0, -10);
    o.r = 2;
    o.barva = color(1, 1, 1);
    o.matnost = 0.0;
    objekty.push_back(o);

    //bílá koule malá
    o.id = 2;
    o.typ = koule_e | svetlo_e;
    o.bod[0] = floatV(1, -0.5, 7);
    o.r = 0.8;
    o.barva = color(1, 1, 1);
    o.matnost = 0.0;
    objekty.push_back(o);

    //žlutá koule
    o.id = 3;
    o.typ = koule_e | svetlo_e;
    o.bod[0] = floatV(1, 1.3, 6);
    o.r = 1;
    o.barva = color(1, 1, 0);
    o.matnost = 1.0;
    objekty.push_back(o);

    //červeno šedá koule
    o.id = 4;
    o.typ = koule_e | svetlo_e;
    o.bod[0] = floatV(0, 0, 4);
    o.r = 1;
    o.barva = color(0.7, 0.5, 0.5);
    o.matnost = 0.5;
    objekty.push_back(o);

    //modré slunce
    o.id = 5;
    o.typ = koule_e | svetlo_e;
    o.bod[0] = floatV(0, 0, 14);
    o.r = 1;
    o.barva = color(0, 0, 1);
    o.matnost = 0.0;
    objekty.push_back(o);

    //tyrkysové slunce
    o.id = 6;
    o.typ = koule_e | svetlo_e;
    o.bod[0] = floatV(10, 0, 0);
    o.r = 1;
    o.barva = color(0, 1, 1);
    o.matnost = 0.0;
    objekty.push_back(o);

    //zmemě
    o.id = 7;
    o.typ = koule_e;
    o.bod[0] = floatV(0, -1002, 0);
    o.r = 1000;
    o.barva = color(0, 0.6, 0);
    o.matnost = 0.0;
    objekty.push_back(o);
}







}
