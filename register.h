#ifndef REGISTER_H
#define REGISTER_H

#include <iostream>
#include <vector>

void register_cpp (int (*p)());
int call_cpps ();

#define REGISTER_MODULE(M) static std::nullptr_t e = ([] () { register_cpp (M); return nullptr; }) ()

#endif
