#ifndef PARTICLE_H
#define PARTICLE_H

#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <omp.h>

#include <vector>

#include "floatV.h"


class Particle
{
public:
    enum typ {
        default_e = 0,
        kulicka_e = (1),
        koralek_e = (1 << 1),
        kyvadlo_e = (1 << 2)
    };

    Particle(floatV pos, floatV speed, float radius = 1.0e-3, uint64_t type = kulicka_e);
    Particle(const Particle& other);
    virtual ~Particle();

    Particle& operator=(Particle& other);
    bool operator==(Particle& other);
    bool operator!=(Particle& other);

    void print();

    virtual void euler_step(float dt, floatV gravity, floatV ext_accell);
    virtual void euler_finalize(float dt);

    void update_reference();

    uint64_t m_gid;
    uint64_t m_cid;
    uint64_t m_type;
    bool m_euler_enabled = true;
    bool m_hidden = false;
    bool m_wall_check_skip = false;
    bool m_collision_skip = false;

    floatV m_pos; // m
    floatV m_pos_old; // m
    floatV m_initial_speed; // m/s

    float m_density = 997.0; // kg/m3
    float m_radius;// = 1.0e-3; // m

    float m_volume;// = 4/3 * 3.1415926 * (m_radius * m_radius * m_radius); // m3
    float m_mass;// = m_volume * m_density; // kg
    float m_inverse_mass;

    floatV m_velocity;
    floatV m_special_velocity;

    float R = 1;
    float G = 1;
    float B = 1;

    float m_lambda = 0;

    omp_lock_t lck;

    std::vector<floatV *> m_pos_reference;
};











#endif // PARTICLE_H
