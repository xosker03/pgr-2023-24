#include "floatV.h"

#include "zaklad/timer.h"

int floatV_self_test(){
    printc(bc.YELLOW, "Self Test: floatV\t");
    int ret = 0;
    bool test;

    {
        test = sizeof(floatV) == 12; ret += !test;
        printif(test, " sizeof ");}

    {
        floatV a;
        test = a.x == 0 && a.y == 0 && a.z == 0; ret += !test;
        printif(test, " zero ");}

    {
        floatV a(1,2,3);
        test = a.x == 1.0 && a.y == 2.0 && a.z == 3.0; ret += !test;
        printif(test, " init ");}

    {
        floatV a(1,2,3);
        floatV b(a);
        test = b.x == 1.0 && b.y == 2.0 && b.z == 3.0; ret += !test;
        printif(test, " cpy ");}

    {
        floatV a(1,2,3);
        floatV c;
        c = a;
        test = c.x == 1.0 && c.y == 2.0 && c.z == 3.0; ret += !test;
        printif(test, " = ");}

    {
        floatV a(1,2,3);
        floatV c(1,2,3);
        test = c == a; ret += !test;
        printif(test, " == ");}

    {
        floatV a(1,2,3);
        floatV b(1,2,3);
        floatV c;
        floatV R(3,6,9);
        c = a + b;
        c += a;
        test = c == R; ret += !test;
        printif(test, " ++= ");}

    {
        floatV a(1,2,3);
        floatV b(1,2,3);
        floatV c(3,6,9);
        floatV R(-1,-2,-3);
        c = a - b;
        c -= a;
        test = c == R; ret += !test;
        printif(test, " --= ");}

    {
        floatV a(1,2,3);
        floatV c;
        floatV R(1*8, 2*8, 3*8);
        c = a * 2;
        c += 2 * a;
        c *= 2;
        test = c == R; ret += !test;
        printif(test, " ***= ");}

    {
        floatV a(1*8, 2*8, 3*8);
        floatV c;
        floatV R(1, 2, 3);
        c = a / 8;
        test = c == R; ret += !test;
        printif(test, " / ");}

    {
        floatV a(1, 2, 3);
        floatV b(2, 1, -1);
        floatV c;
        floatV R(-5, 7, -3);
        c = a ^ b;
        test = c == R; ret += !test;
        printif(test, " cross ");}

     {
        floatV a(1, 2, 1);
        floatV b(2, 1, -1);
        float c;
        float R = 3;
        c = a & b;
        test = c == R; ret += !test;
        printif(test, " scalar ");}

    {
        floatV a(1, 2, 3);
        float R1 = 3.741;
        float R2 = 3.742;
        test = a.len() > R1 && a.len() < R2; ret += !test;
        printif(test, " len ");}

    {
        floatV a(1, 2, 3);
        float R1 = 14;
        test = a.lenSq() == R1; ret += !test;
        printif(test, " lenSq ");}

    {
        floatV a(4, 4, 2);
        floatV R1(4.0f/6.0f, 4.0f/6.0f, 2.0f/6.0f);
        test = a.norm() == R1; ret += !test;
        printif(test, " norm ");}

    {
        floatV a(1, 2, 3);
        floatV b(2, 1, -1);
        floatV c;
        floatV R(-5, 7, -3);
        c = (a + b) ^ (b * (a & b));
        test = c == R; ret += !test;
        printif(test, " FINAL ");}

    // {
    //     Timer t;
    //     floatV a(1,2,3);
    //     floatV b(4,5,6);
    //     floatV c;
    //     t.start();
    //     for(int i = 0; i < 10000000; ++i){
    //         c += a + b * 3;
    //     }
    //     t.stop();
    //     printgreen(" SPD:%fms ", t.ms());
    //     printdgreen("%s", c.str().c_str());
    // }


    printf("\n");
    return ret;
}
