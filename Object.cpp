#include "Object.h"



Object::Object()
{
}

// Object::Object(const Object& other)
// {
// }

Object::~Object()
{
    //printf("Destruktor %p\n", this);
    if(!pseudo_object)
        for(auto a : m_particles)
            delete a;
    for(auto a : m_subobjects)
        a->~Object();
}

// Object& Object::operator=(Object& other)
// {
//     return *this;
// }
//
// bool Object::operator==(Object& other)
// {
//     return false;
// }
//
// bool Object::operator!=(Object& other)
// {
//     return ! ((*this) == other);
// }


void Object::print_info(int level)
{
    if(!level)
        printc(bc.CYAN);
    else
        printc(bc.DCYAN);
    for(int i = 0; i < level; ++i)
        printf("\t");
    printf("%s:", my_name().c_str());

    printc(bc.BLUE);
    for(auto & a : m_particles)
        printf(" %lu", a->m_gid);
    printc(bc.ENDC);
    printf("\n");
    for(auto & a : m_subobjects)
        a->print_info(level + 1);
}

