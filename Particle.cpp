#include "Particle.h"



static uint64_t global_index_counter = 0;


Particle::Particle(floatV pos, floatV speed, float radius, uint64_t type)
{
    m_gid = global_index_counter++;
    m_pos = pos + floatV(25,2,25);
    m_initial_speed = speed;

    m_radius = radius; // m
    m_volume = 4/3 * 3.1415926 * (m_radius * m_radius * m_radius); // m3
    m_mass = m_volume * m_density; // kg
    m_inverse_mass = 1 / m_mass;

    m_type = type;

    omp_init_lock(&lck);
}

Particle::Particle(const Particle& other)
{
    m_mass = other.m_mass;
    m_gid = other.m_gid;
    m_cid = other.m_cid;
    m_type = other.m_type;
    m_euler_enabled = other.m_euler_enabled;
    m_hidden = other.m_hidden;
    m_pos = other.m_pos;
    m_pos_old = other.m_pos_old;
    m_initial_speed = other.m_initial_speed;
    m_density = other.m_density;
    m_radius = other.m_radius;
    m_volume = other.m_volume;
    m_mass = other.m_mass;
    m_inverse_mass = other.m_inverse_mass;
    floatV m_velocity = other.m_velocity;
    R = other.R;
    G = other.G;
    B = other.B;
    lck = other.lck;
}

Particle::~Particle()
{

}

Particle& Particle::operator=(Particle& other)
{
    m_pos = other.m_pos;
    return *this;
}

bool Particle::operator==(Particle& other)
{
    //return m_pos == other.m_pos && m_pos_old == other.m_pos_old;
    return m_gid == other.m_gid;
}

bool Particle::operator!=(Particle& other)
{
    return ! ((*this) == other);
}



void Particle::print(){
    printf("id: %lu,\tpos: %f %f,\t V: %f %f, r: %f, M %f, E: %d\n",
           m_gid,
           m_pos.x, m_pos.y,
           m_velocity.x, m_velocity.y,
           m_radius,
           m_mass,
           m_euler_enabled);
}




void Particle::euler_step(float dt, floatV gravity, floatV ext_accell)
{
    if(!m_euler_enabled)
        return;
    m_pos_old = m_pos;
    m_velocity += dt * ext_accell;
    m_velocity += dt * gravity;
    m_velocity += dt * m_special_velocity;
    m_pos = m_pos + dt * m_velocity;
}


void Particle::euler_finalize(float dt)
{
    if(!m_euler_enabled)
        return;
    m_velocity = m_pos - m_pos_old;
    m_velocity *= 1/dt;
}

void Particle::update_reference()
{
    for(auto & a : m_pos_reference){
        // printf("*ref, ref: %p, %p\n", &m_pos_reference, a);
        *a = m_pos - floatV(25,2,25);
    }
}



#include "register.h"
static int self_test(){
    printc(bc.YELLOW, "Self Test: Particle\t");

    printf("\n");
    return 0;
}
//REGISTER_MODULE(self_test);

