#ifndef OBJECT_H
#define OBJECT_H

#include <stdio.h>
#include <stdint.h>

#include <vector>

#include "main.h"
#include "Particle.h"


class Object
{
public:
    Object();
    //Object(const Object& other);
    virtual ~Object();
    virtual std::string my_name() {return "Objekt";};

    void print_info(int level = 0);

    // Object& operator=(Object& other);
    // bool operator==(Object& other);
    // bool operator!=(Object& other);

    virtual void init_constraints() {};
    virtual void solve_constraints() {};
    virtual void compute_vorticity() {};
    virtual void gl_draw() {};

    int m_sdt = 10;
    float c_alpha = 0;

    std::vector<Particle *> m_particles;
    std::vector<Object *> m_subobjects;

    std::vector<floatV *> m_reference;

protected:
    bool pseudo_object = false;
};











#endif // CONSTRAINT_H
