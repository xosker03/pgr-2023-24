#include "Simulation.h"

#include <unistd.h>

#include <iostream>
#include <algorithm>

#include "zaklad/zaklad.h"

#include "main.h"

#include "Object/Ball.h"
#include "Object/Bead.h"
#include "Object/Pendulum.h"
#include "Object/Triangle.h"
#include "Object/Bunny.h"
#include "Object/Tetrahedron.h"
#include "Object/Water.h"
#include "Object/Cloth.h"
#include "Object/Cube.h"
#include "Object/Robotarm.h"

#include "Particle/Anchor.h"

Simulation::Simulation(bool sync, int typ_kulicek)
{
    sim_dt = &m_dt;
    sync_gl = sync;


    if(typ_kulicek == -2){
        //PRÁZDNÉ PRO SELFTEST
    }
    if(typ_kulicek == -1){
        m_objects.push_back( new Ball (floatV(0.65f, 0.5f, 0.2f), floatV(0.0, 0.0), 0.03) );
        m_objects.push_back( new Ball (floatV(0.3f, 0.5f, 0.2f), floatV(5.0, 0.0), 0.03) );
        m_objects.push_back( new Ball (floatV(0.2f, 0.5f, 0.2f), floatV(-5.0, 0.0), 0.03) );

        // m_data.push_back( new Anchor (floatV(0.5f, 0.8f, 0.2f), floatV(0.0, 0.0), 0.05) );
    }

    if(typ_kulicek == 0){
        m_data.push_back( new Particle ((floatV) {(200) * 1.0e-3f, (900) * 1.0e-3f, 0.2f}, (floatV) {0.0, 0.0}, 0.025) );
        m_data.push_back( new Particle ((floatV) {(100) * 1.0e-3f, (100) * 1.0e-3f, 0.2f}, (floatV) {5.0, 5.0}, 0.05) );
        m_data.push_back( new Particle ((floatV) {(800) * 1.0e-3f, (800) * 1.0e-3f, 0.2f}, (floatV) {-5.0, -5.0}, 0.1) );
    }

    if(typ_kulicek == 1){
        for(float i = 0; i < 5; ++i){
            for(float j = 0; j < 5; ++j){
                Particle * k = new Particle ((floatV) {i * 1.0e-1f + 0.1f, j * 1.0e-1f + 0.5f, 0.2f}, (floatV) {i / 1.0f, j / 10.0f}, (i + j + 1) * 0.005f);
                m_data.push_back(k);
            }
        }
    }

    if(typ_kulicek == 2){
        for(float i = 0; i < 35; ++i){
            for(float j = 0; j < 35; ++j){
                for(float k = 0; k < 1; ++k){
                    m_objects.push_back( new Ball(floatV(i * 2.0e-2f + 0.1f, j * 2.0e-2f + 0.2f, k * 2.0e-2f + 0.2f),
                                floatV(i / 10.0f, j / 100.0f),
                                ( 1) * 0.005f)
                    );
                }
            }
        }
        m_objects.push_back( new Ball( floatV(0.5f, 0.5f, 0.7f), floatV(0, 0, -5), 0.05) );
        m_objects.back()->m_particles.front()->R = 1;
        m_objects.back()->m_particles.front()->G = 0;
        m_objects.back()->m_particles.front()->B = 0;
        m_objects.back()->m_particles.front()->m_type = Particle::default_e;
    }

    if(typ_kulicek == 3){
        m_objects.push_back( new Bead ((floatV) {0.65f, 0.5f, 0.2f}, (floatV) {0.0, 0.0}, 0.03) );
        m_objects.push_back( new Bead ((floatV) {0.35f, 0.5f, 0.2f}, (floatV) {0.0, 2.0}, 0.03) );
        m_objects.push_back( new Bead ((floatV) {0.5f, 0.65f, 0.2f}, (floatV) {1.0, 0.0}, 0.03) );

        for(int i = 0; i < 10; ++i)
            m_data.push_back( new Particle ((floatV) {i * 1.0e-1f + 0.05f, 0.8f, 0.2f}, (floatV) {i * 4.0f / 10.0f, i / 10.0f}, 0.02) );
    }

    if(typ_kulicek == 4){
        // m_objects.push_back( new Bead( floatV(0.35f, 0.5f, 0.2f), floatV(0.0, 0.0), 0.03) );
        // m_objects.push_back( new Bead( floatV(0.5f, 0.65f, 0.2f), floatV(1.0, 0.0), 0.03) );
        m_objects.push_back( new Bead(
            new Particle(floatV(0.35f, 0.5f, 0.2f), floatV(0.0, 0.0), 0.03),
            new Anchor(floatV(0.5, 0.5, 0.2f), floatV(), 0.01))
        );
        // m_objects.back()->m_sdt = 10;
        m_objects.push_back( new Bead(
            new Particle(floatV(0.5f, 0.65f, 0.2f), floatV(1.0, 0.0), 0.03),
            new Anchor(floatV(0.5, 0.5, 0.2f), floatV(), 0.01))
        );
        // m_objects.back()->m_sdt = 10;
        m_objects.push_back( new Pendulum( (floatV) {0.5f, 0.5f, 0.2f}, (floatV) {1.0, 0.0}, 0.03) );
        for(int i = 0; i < 10; ++i)
            m_objects.push_back( new Ball( (floatV) {i * 1.0e-1f + 0.05f, 0.8f, 0.2f}, (floatV) {i * 4.0f / 10.0f, i / 10.0f}, 0.02) );
        m_objects.push_back( new Triangle( floatV(0.5f, 0.5f, 0.2f), floatV(1.0, 0.0), 0.03) );
    }


    if(typ_kulicek == 5){
        m_objects.push_back( new Bunny( floatV(3,5,10), floatV(0, -25, 0), 0.002f) );
        // m_objects.push_back( new Ball( floatV(2.0f, 2.0f, 2.0f), floatV(0, 0, 0), 0.05) );
        // m_objects.back()->m_particles.front()->R = 1;
        // m_objects.back()->m_particles.front()->G = 1;
        // m_objects.back()->m_particles.front()->B = 0;
        // m_objects.back()->m_particles.front()->m_type = Particle::default_e;
        // m_objects.back()->m_particles.front()->m_euler_enabled = false;
        // m_objects.back()->m_particles.front()->m_wall_check_skip = true;
        m_objects.push_back( new Cloth( floatV(-10,15,10), floatV(-13.5,5,15), 0.002f) );
    }

    if(typ_kulicek == 6){
        m_objects.push_back( new Tetrahedron( floatV(0.5f, 0.5f, 0.5f), floatV(), 0.005f) );
    }

    if(typ_kulicek == 7){
        m_objects.push_back( new Water( floatV(0.0f, 0.0f, 0.0f), floatV(), 0.008f, this) );
    }

    if(typ_kulicek == 8){
        for(float i = 0; i < 24; ++i){
            for(float j = 0; j < 24; ++j){
                for(float k = 0; k < 24; ++k){
                    m_objects.push_back( new Ball(
                        floatV(i * 2.0e-2f, j * 2.0e-2f, k * 2.0e-2f) + floatV(0.2, 0.5, 0.2) + floatV(0.005f * ((int)j % 2), 0.005f, 0.005f * ((int)j % 2)),
                        floatV(0.0f, 0.0f, 0.0f),
                        0.01f)
                    );
                }
            }
        }
    }

    if(typ_kulicek == 9){
        m_objects.push_back( new Cloth( floatV(0.4f, 0.8f, 0.5f), floatV(), 0.001f) );
    }


    if(typ_kulicek == 10){
        m_objects.push_back( new Robotarm( floatV(0.4f, 0.4f, 0.2f), floatV(), 0.01f) );
    }

    if(typ_kulicek == 11){
        m_objects.push_back( new Cube( floatV(0.4f, 0.4f, 0.2f), floatV(), 0.002f) );
    }





    // if(typ_kulicek != -2)
    //     for(auto & a : m_objects)
    //         a->print_info();

    init();
}

Simulation::~Simulation()
{
    for(auto & a : m_objects){
        delete a;
    }
}

float Simulation::gravity_force(float weight)
{
    return (6.67e-11 * 5.972e24 * weight) / (6371.0e3 * 6371.0e3);
}
float Simulation::acceleration(float force, float weight)
{
    return force / weight;
}


void Simulation::init()
{
    m_sett.center = (m_sett.wall_l + m_sett.wall_r) / 2;

    for(auto & a : m_objects){
        for(auto & b : a->m_particles){
            if(b->m_hidden)
                continue;
            m_data.push_back(b);
            //b.print();
        }
    }

    for(auto & a : m_data){
        a->m_velocity = a->m_initial_speed;
    }

    // for(auto & a : m_data){
    //     a->print();
    // }

    for(auto & a : m_data){
        max_diameter = std::max(a->m_radius * 4, max_diameter);
    }
    cells_dim[0] = (m_sett.wall_r.x - m_sett.wall_l.x) / max_diameter + 0.5;
    cells_dim[1] = (m_sett.wall_r.y - m_sett.wall_l.y) / max_diameter + 0.5;
    cells_dim[2] = (m_sett.wall_r.z - m_sett.wall_l.z) / max_diameter + 0.5;
    cells_dim[0] = 10;
    cells_dim[1] = 10;
    cells_dim[2] = 10;
    cells = cells_dim[0] * cells_dim[1] * cells_dim[2];
    cells_starts.resize(cells);
}


void Simulation::run()
{
    //int snimek = 0;
    while(m_time < m_end_time){
        while(m_sim_pause);
        step();
        //usleep(1000000 / 6000);
    }
}


int64_t Simulation::tocid(Particle & a){
    auto & pos = a.m_pos;
    int64_t x = pos.x / max_diameter;
    int64_t y = pos.y / max_diameter;
    int64_t z = pos.z / max_diameter;
    return tocid(x, y, z);
}
int64_t Simulation::tocid(int64_t x, int64_t y, int64_t z){
    int64_t cid = 0;
    cid = x + y * cells_dim[0] + z * cells_dim[0] * cells_dim[1];
    if(cid >= cells){
        // printc(bc.RED, "CID %ld/%lu\n", cid, cells);
        cid = cells - 1;
    }
    if(cid < 0){
        // printc(bc.RED, "CID %ld/%lu\n", cid, cells);
        cid = 0;
    }
    return cid;
}
std::tuple<int64_t, int64_t, int64_t> Simulation::fromcid(Particle & a)
{
    auto & pos = a.m_pos;
    int64_t x = pos.x / max_diameter;
    int64_t y = pos.y / max_diameter;
    int64_t z = pos.z / max_diameter;
    return std::tuple<int64_t, int64_t, int64_t>(x,y,z);
}
std::vector<uint64_t> Simulation::adjecentcid(Particle & a){
    std::vector<uint64_t> ret;

    auto [x, y, z] = fromcid(a);

    for(int64_t xi = x-1; xi <= x+1; ++xi)
        for(int64_t yi = y-1; yi <= y+1; ++yi)
            for(int64_t zi = z-1; zi <= z+1; ++zi)
                if(xi >= 0 && yi >= 0 && zi >= 0 && xi < cells_dim[0] && yi < cells_dim[1] && zi < cells_dim[2])
                    ret.push_back(tocid(xi, yi, zi));
    return ret;
}


bool Simulation::step()
{
    printc(bc.YELLOW, "\r%f/%f\t", m_time, m_end_time);
    printc(bc.GRAY, "E: %.3e ", m_energy);
    printc(bc.DRED, "SPS: %.1f\t", 1000.0 / timer.ms());

    // if(m_time >= m_end_time){
    //     // getchar();
    //     // exit(0);
    //     return 0;
    // }

    while(m_sim_pause){
        return 0;
    };

    timer.start();

    //#pragma omp parallel for
    for(int idx = 0; idx < m_data.size(); ++idx){
        auto & ball1 = *m_data[idx];
        ball1.euler_step(m_dt, m_sett.gravity_accell * m_gravity_enabled, m_ext_acceleration);
    }

    //// GPU sousednost ////
    for(auto & a : m_data){
        a->m_cid = tocid(*a);
        auto [x,y,z] = fromcid(*a);
        a->R = x / (cells_dim[0] * 0.8) + 0.1;
        a->G = y / (cells_dim[1] * 0.8) + 0.1;
        a->B = z / (cells_dim[2] * 0.8) + 0.1;
        a->m_type = Particle::default_e;
    }
    std::sort (m_data.begin(), m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_cid < b->m_cid);});
    uint64_t last_cid = -1;
    for(int i = 0; i < m_data.size(); ++i){
        if(m_data[i]->m_cid != last_cid){
            last_cid = m_data[i]->m_cid;
            cells_starts[last_cid] = i;
        }
    }


    for(auto & a : m_objects){
        a->solve_constraints();
    }
    for(int idx = 0; idx < m_data.size(); ++idx){
        auto & ball1 = *m_data[idx];
        ball1.euler_finalize(m_dt);
    }

    if(!use_simple_collisions){
        //// GPU kolize ////
        #pragma omp parallel for firstprivate(m_data) schedule(dynamic,100)
        for(int idx = 0; idx < m_data.size(); ++idx){
            auto adjecent = adjecentcid(*m_data[idx]);
            for(auto adj : adjecent){
                int j = cells_starts[adj];
                auto local_b_cid = adj;
                for(; j < m_data.size() && m_data[j]->m_cid == local_b_cid; ++j){
                    if(m_data[idx] != m_data[j]){
                        handleCollisions(*m_data[idx], *m_data[j]);
                    }
                }
            }
            handleBorders(*m_data[idx]);
        }
    ///////////////////
    } else {
        for(int idx = 0; idx < m_data.size(); ++idx){
            auto & ball1 = m_data[idx];
            //colisions
            for(int jdx = idx + 1; jdx < m_data.size(); ++jdx){
                auto & ball2 = *m_data[jdx];
                handleCollisions(*ball1, ball2);
            }
            //wall collisions
            handleBorders(*ball1);
        }
    }

    for(auto & a : m_objects){
        // a->compute_vorticity();
    }

    m_energy = 0;
    for(auto & a : m_data){
        m_energy += 0.5 * a->m_mass * a->m_velocity.len() * a->m_velocity.len();
        m_energy += a->m_mass * a->m_pos.y * 9.81 * m_gravity_enabled;
        a->update_reference();
    }

    timer.stop();
    step_time_ms = timer.ms();

    m_time += m_dt;
    return m_time < m_end_time;
}



void Simulation::handleCollisions(Particle & ball1, Particle & ball2)
{
    if(ball1 == ball2)
        return;
    if(ball1.m_collision_skip || ball2.m_collision_skip)
        return;
    if(ball1.m_gid < ball2.m_gid){
        omp_set_lock(&ball1.lck);
        omp_set_lock(&ball2.lck);
    } else {
        omp_set_lock(&ball2.lck);
        omp_set_lock(&ball1.lck);
    }

    floatV direction = ball2.m_pos - ball1.m_pos;
    float distance = direction.len();
    if(distance == 0.0 || distance == -0.0 || distance > ball1.m_radius + ball2.m_radius){
        omp_unset_lock(&ball2.lck);
        omp_unset_lock(&ball1.lck);
        return;
    }
    if(abs(distance - (ball1.m_radius + ball2.m_radius)) < 1e-7){
        omp_unset_lock(&ball2.lck);
        omp_unset_lock(&ball1.lck);
        return;
    }

    //normalizace
    direction *= 1.0f / distance;

    float correction = (ball1.m_radius + ball2.m_radius - distance) / 2.0f;

    ball1.m_pos += direction * (-correction);
    ball2.m_pos += direction * correction;

    //rychlost
    float v1 = ball1.m_velocity & direction;
    float v2 = ball2.m_velocity & direction;

    float m1 = ball1.m_mass;
    float m2 = ball2.m_mass;

    float e = m_sett.restitution;
    float newv1 = (m1*v1 + m2*v2 - m2*(v1 - v2) * e) / (m1 + m2);
    float newv2 = (m1*v1 + m2*v2 - m1*(v2 - v1) * e) / (m1 + m2);


    ball1.m_velocity += direction * (newv1 - v1);
    ball2.m_velocity += direction * (newv2 - v2);


    omp_unset_lock(&ball2.lck);
    omp_unset_lock(&ball1.lck);
}

void Simulation::handleBorders(Particle & a)
{
    if(a.m_wall_check_skip){
        return;
    }
    if(a.m_pos.x - a.m_radius < m_sett.wall_l.x){
        a.m_pos.x = m_sett.wall_l.x + a.m_radius;
        a.m_velocity.x *= -m_sett.dumping;
        a.m_velocity *= m_sett.dumping_perpen;
    }
    if(a.m_pos.x + a.m_radius > m_sett.wall_r.x){
        a.m_pos.x = m_sett.wall_r.x - a.m_radius;
        a.m_velocity.x *= -m_sett.dumping;
        a.m_velocity *= m_sett.dumping_perpen;
    }
    if(a.m_pos.y - a.m_radius < m_sett.wall_l.y){
        a.m_pos.y = m_sett.wall_l.y + a.m_radius;
        a.m_velocity.y *= -m_sett.dumping;
        a.m_velocity *= m_sett.dumping_perpen;
    }
    if(a.m_pos.y + a.m_radius > m_sett.wall_r.y){
        a.m_pos.y = m_sett.wall_r.y - a.m_radius;
        a.m_velocity.y *= -m_sett.dumping;
        a.m_velocity *= m_sett.dumping_perpen;
    }
    if(a.m_pos.z - a.m_radius < m_sett.wall_l.z){
        a.m_pos.z = m_sett.wall_l.z + a.m_radius;
        a.m_velocity.z *= -m_sett.dumping;
        a.m_velocity *= m_sett.dumping_perpen;
    }
    if(a.m_pos.z + a.m_radius > m_sett.wall_r.z){
        a.m_pos.z = m_sett.wall_r.z - a.m_radius;
        a.m_velocity.z *= -m_sett.dumping;
        a.m_velocity *= m_sett.dumping_perpen;
    }
}



///////////////////////////////////////////////////////////////////////////////////////////////


#include "register.h"




#define SIMPREP(SIMM) \
    Simulation SIMM(false, -2); \
    SIMM.m_gravity_enabled = 1.0f; \
    SIMM.m_gravity_enabled = 1.0f; \
    SIMM.m_sim_pause = false; \
    SIMM.m_ext_acceleration = floatV(0,0); \
    SIMM.m_energy = 0; \
    SIMM.m_dt = 1.0e-3f; \
    SIMM.m_time = 0;  \
    SIMM.m_end_time = 1.5; \
    SIMM._wire = floatV(0.5f, 0.5f); \
    SIMM._wireRadius = 0.15f; \
    SIMM.m_sett = { \
        .gravity_accell = floatV(0, -9.81), \
        .dumping_perpen = 0.999f, \
        .dumping = (0.75f + (1 - 0.999f)), \
        .restitution = 0.99f, \
        .wall_l = floatV(0.0, 0.0, 0.0), \
        .wall_r = floatV(1.0, 1.0, 1.0) \
    }; \
    SIMM.max_diameter = 0;



static int self_test(){
    printc(bc.YELLOW, "Self Test: Simulation\t");
    omp_set_num_threads (1);

    int ret = 0;
    bool test;



    { //3 kuličky statické
        double max_error = 1e-5;
        SIMPREP(SIM);
        SIM.m_objects.push_back( new Ball ((floatV) {(200) * 1.0e-3f, (900) * 1.0e-3f, 0.2f}, (floatV) {0.0, 0.0}, 0.025) );
        SIM.m_objects.push_back( new Ball ((floatV) {(100) * 1.0e-3f, (100) * 1.0e-3f, 0.2f}, (floatV) {5.0, 5.0}, 0.05) );
        SIM.m_objects.push_back( new Ball ((floatV) {(800) * 1.0e-3f, (800) * 1.0e-3f, 0.2f}, (floatV) {-5.0, -5.0}, 0.1) );

        //SIM.use_simple_collisions = true;
        SIM.init();
        SIM.run();

        floatV RPos[] = {
            floatV(3.952082e-01, 9.203170e-02, 2.000000e-01),
            floatV(4.034313e-01, 3.006248e-01, 2.000000e-01),
            floatV(5.761085e-01, 1.420439e-01, 2.000000e-01),
        };
        floatV RVel[] = {
            floatV(-1.023978e+00, 2.174079e+00, 0.000000e+00),
            floatV(1.657963e+00, 2.630442e+00, 0.000000e+00),
            floatV(7.215738e-01, -7.837563e-01, 0.000000e+00),
        };

        float acc = 0;
        float acc2 = 0;

        for(int i = 0; i < SIM.m_data.size(); ++i){
            acc += (SIM.m_data[i]->m_pos - RPos[i]).len();
            acc2 += (SIM.m_data[i]->m_velocity - RVel[i]).len();
            //SIM.m_data[i]->m_pos.print();
            //SIM.m_data[i]->m_velocity.print();
        }

        test = acc < max_error;
        test = test && (acc2 < max_error);
        ret += !test;
        printif(test, "| Test 1: %.3e %.3e |", acc, acc2);
    }

    { //Kyvadlo a další blbosti statické
        double max_error = 1e2;
        SIMPREP(SIM);
        // SIM.m_objects.push_back( new Bead( floatV(0.35f, 0.5f, 0.2f), floatV(0.0, 0.0), 0.03) );
        // SIM.m_objects.push_back( new Bead( floatV(0.5f, 0.65f, 0.2f), floatV(1.0, 0.0), 0.03) );
        SIM.m_objects.push_back( new Bead(
            new Particle(floatV(0.35f, 0.5f, 0.2f), floatV(0.0, 0.0), 0.03),
                                      new Anchor(floatV(0.5, 0.5, 0.2f), floatV(), 0.01))
        );
        SIM.m_objects.push_back( new Bead(
            new Particle(floatV(0.5f, 0.65f, 0.2f), floatV(1.0, 0.0), 0.03),
                                      new Anchor(floatV(0.5, 0.5, 0.2f), floatV(), 0.01))
        );
        SIM.m_objects.push_back( new Pendulum( floatV(0.5f, 0.5f, 0.2f), floatV(1.0, 0.0), 0.03) );
        for(int i = 0; i < 10; ++i)
            SIM.m_objects.push_back( new Ball( floatV(i * 1.0e-1f + 0.05f, 0.8f, 0.2f), floatV(i * 4.0f / 10.0f, i / 10.0f), 0.02) );
        SIM.m_objects.push_back( new Triangle( floatV(0.5f, 0.5f, 0.2f), floatV(1.0, 0.0), 0.03) );

        //SIM.use_simple_collisions = true;
        SIM.init();
        SIM.run();

        floatV RPos[] = {
            floatV(1.975139e-01, 3.015892e-02, 2.000000e-01),
            floatV(3.943814e-01, 1.188930e-01, 2.000000e-01),
            floatV(7.018977e-01, 1.688354e-02, 2.000000e-01),
            floatV(6.627884e-01, 4.739840e-02, 2.000000e-01),
            floatV(7.361431e-01, 3.532961e-02, 2.000000e-01),
            floatV(7.412571e-01, 7.120512e-02, 2.000000e-01),
            floatV(7.689705e-01, 1.798895e-02, 2.000000e-01),
            floatV(7.826641e-01, 5.457341e-02, 2.000000e-01),
            floatV(8.760897e-01, 3.247997e-02, 2.000000e-01),
            floatV(8.524480e-01, 8.969499e-02, 2.000000e-01),
            floatV(9.289929e-01, 3.540758e-02, 2.000000e-01),
            floatV(9.689513e-01, 4.365189e-02, 2.000000e-01),
            floatV(7.590449e-02, 3.495347e-01, 2.000000e-01),
            floatV(3.611251e-01, 2.651599e-01, 2.000000e-01),
            floatV(4.842426e-01, 3.508275e-01, 2.000000e-01),
            floatV(5.439657e-01, 3.565855e-01, 2.000000e-01),
            floatV(9.385713e-01, 2.787756e-01, 2.000000e-01),
            floatV(4.211473e-01, 3.723980e-01, 2.000000e-01),
        };

        float acc = 0;

        for(int i = 0; i < SIM.m_data.size(); ++i){
            acc += (SIM.m_data[i]->m_pos - RPos[i]).len();
            // SIM.m_data[i]->m_pos.print();
        }

        test = acc < max_error;
        ret += !test;
        printif(test, "| Test 2: %.3e |", acc);
    }


    { //Scénář 0 Paralel vs Simple
        double max_error = 1e-5;
        SIMPREP(SIM);
        SIMPREP(SIM2);
        SIM.m_objects.push_back( new Ball ((floatV) {(200) * 1.0e-3f, (900) * 1.0e-3f, 0.2f}, (floatV) {0.0, 0.0}, 0.025) );
        SIM.m_objects.push_back( new Ball ((floatV) {(100) * 1.0e-3f, (100) * 1.0e-3f, 0.2f}, (floatV) {5.0, 5.0}, 0.05) );
        SIM.m_objects.push_back( new Ball ((floatV) {(800) * 1.0e-3f, (800) * 1.0e-3f, 0.2f}, (floatV) {-5.0, -5.0}, 0.1) );
        SIM2.m_objects.push_back( new Ball ((floatV) {(200) * 1.0e-3f, (900) * 1.0e-3f, 0.2f}, (floatV) {0.0, 0.0}, 0.025) );
        SIM2.m_objects.push_back( new Ball ((floatV) {(100) * 1.0e-3f, (100) * 1.0e-3f, 0.2f}, (floatV) {5.0, 5.0}, 0.05) );
        SIM2.m_objects.push_back( new Ball ((floatV) {(800) * 1.0e-3f, (800) * 1.0e-3f, 0.2f}, (floatV) {-5.0, -5.0}, 0.1) );

        SIM.m_end_time = 150.0;
        SIM2.m_end_time = SIM.m_end_time;

        //SIM.use_simple_collisions = true;
        SIM2.use_simple_collisions = true;
        SIM.init();
        SIM2.init();

        float acc = 0;
        float acc2 = 0;

        while(SIM.m_time < SIM.m_end_time){
            SIM.step();
            SIM2.step();
            acc = 0;
            acc2 = 0;
            std::sort (SIM.m_data.begin(), SIM.m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_gid < b->m_gid);});
            std::sort (SIM2.m_data.begin(), SIM2.m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_gid < b->m_gid);});
            for(int i = 0; i < SIM.m_data.size(); ++i){
                acc += (SIM.m_data[i]->m_pos - SIM2.m_data[i]->m_pos).len();
                acc2 += (SIM.m_data[i]->m_velocity - SIM2.m_data[i]->m_velocity).len();
            }
            if(acc >= max_error || acc2 >= max_error)
                break;
        }

        test = SIM.m_time >= 5.0;
        ret += !test;
        printif(test, "| Test 3: %f |", SIM.m_time);
    }


    { //Hodně kuliček Paralel vs Simple
        double max_error = 1e3;
        SIMPREP(SIM);
        SIMPREP(SIM_simple);
        for(float i = 0; i < 6; ++i){
            for(float j = 0; j < 6; ++j){
                for(float k = 0; k < 6; ++k){
                    SIM.m_objects.push_back( new Ball(
                        floatV(i * 2.0e-2f, j * 2.0e-2f, k * 2.0e-2f) + floatV(0.2, 0.5, 0.2) + floatV(0.005f * ((int)j % 2), 0.005f, 0.005f * ((int)j % 2)),
                        floatV(0.0f, 0.0f, 0.0f),
                        0.01f)
                    );
                    SIM_simple.m_objects.push_back( new Ball(
                        floatV(i * 2.0e-2f, j * 2.0e-2f, k * 2.0e-2f) + floatV(0.2, 0.5, 0.2) + floatV(0.005f * ((int)j % 2), 0.005f, 0.005f * ((int)j % 2)),
                        floatV(0.0f, 0.0f, 0.0f),
                        0.01f)
                    );
                }
            }
        }

        SIM.m_end_time = 0.5;
        SIM_simple.m_end_time = SIM.m_end_time;

        // SIM.use_simple_collisions = true;
        SIM_simple.use_simple_collisions = true;
        SIM.init();
        SIM_simple.init();

        SIM.run();
        SIM_simple.run();

        std::sort (SIM.m_data.begin(), SIM.m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_gid < b->m_gid);});
        std::sort (SIM_simple.m_data.begin(), SIM_simple.m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_gid < b->m_gid);});

        float acc = 0;
        float acc2 = 0;

        for(int i = 0; i < SIM.m_data.size(); ++i){
            acc += (SIM.m_data[i]->m_pos - SIM_simple.m_data[i]->m_pos).len();
            acc2 += (SIM.m_data[i]->m_velocity - SIM_simple.m_data[i]->m_velocity).len();
            // SIM.m_data[i]->m_pos.print();
            // SIM_simple.m_data[i]->m_pos.print();
            // printf("%lu %lu\n", SIM.m_data[i]->m_gid, SIM_simple.m_data[i]->m_gid);
        }

        test = acc < max_error;
        test = test && (acc2 < max_error);
        ret += !test;
        printif(test, "| Test 4: %.3e %.3e |", acc, acc2);
        omp_set_num_threads (omp_get_num_procs());
    }

    { //Hodně kuliček Detekce náhody
        double max_error = 1e3;
        SIMPREP(SIM);
        SIMPREP(SIM_2);
        for(float i = 0; i < 6; ++i){
            for(float j = 0; j < 6; ++j){
                for(float k = 0; k < 6; ++k){
                    SIM.m_objects.push_back( new Ball(
                        floatV(i * 2.0e-2f, j * 2.0e-2f, k * 2.0e-2f) + floatV(0.2, 0.5, 0.2) + floatV(0.005f * ((int)j % 2), 0.005f, 0.005f * ((int)j % 2)),
                                                      floatV(0.0f, 0.0f, 0.0f),
                                                      0.01f)
                    );
                    SIM_2.m_objects.push_back( new Ball(
                        floatV(i * 2.0e-2f, j * 2.0e-2f, k * 2.0e-2f) + floatV(0.2, 0.5, 0.2) + floatV(0.005f * ((int)j % 2), 0.005f, 0.005f * ((int)j % 2)),
                                                             floatV(0.0f, 0.0f, 0.0f),
                                                             0.01f)
                    );
                }
            }
        }

        SIM.m_end_time = 0.5;
        SIM_2.m_end_time = SIM.m_end_time;

        // SIM.use_simple_collisions = true;
        SIM.init();
        SIM_2.init();

        SIM.run();
        SIM_2.run();

        std::sort (SIM.m_data.begin(), SIM.m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_gid < b->m_gid);});
        std::sort (SIM_2.m_data.begin(), SIM_2.m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_gid < b->m_gid);});

        float acc = 0;
        float acc2 = 0;

        for(int i = 0; i < SIM.m_data.size(); ++i){
            acc += (SIM.m_data[i]->m_pos - SIM_2.m_data[i]->m_pos).len();
            acc2 += (SIM.m_data[i]->m_velocity - SIM_2.m_data[i]->m_velocity).len();
            // SIM.m_data[i]->m_pos.print();
            // SIM_simple.m_data[i]->m_pos.print();
            // printf("%lu %lu\n", SIM.m_data[i]->m_gid, SIM_simple.m_data[i]->m_gid);
        }

        test = acc < max_error;
        test = test && (acc2 < max_error);
        ret += !test;
        printif(test, "| Test 5: %.3e %.3e |", acc, acc2);
        omp_set_num_threads (omp_get_num_procs());
    }

    { //Hodně kuliček Simple Detekce náhody
        double max_error = 1e-5;
        SIMPREP(SIM);
        SIMPREP(SIM_2);
        for(float i = 0; i < 6; ++i){
            for(float j = 0; j < 6; ++j){
                for(float k = 0; k < 6; ++k){
                    SIM.m_objects.push_back( new Ball(
                        floatV(i * 2.0e-2f, j * 2.0e-2f, k * 2.0e-2f) + floatV(0.2, 0.5, 0.2) + floatV(0.005f * ((int)j % 2), 0.005f, 0.005f * ((int)j % 2)),
                                                      floatV(0.0f, 0.0f, 0.0f),
                                                      0.01f)
                    );
                    SIM_2.m_objects.push_back( new Ball(
                        floatV(i * 2.0e-2f, j * 2.0e-2f, k * 2.0e-2f) + floatV(0.2, 0.5, 0.2) + floatV(0.005f * ((int)j % 2), 0.005f, 0.005f * ((int)j % 2)),
                                                        floatV(0.0f, 0.0f, 0.0f),
                                                        0.01f)
                    );
                }
            }
        }

        SIM.m_end_time = 0.5;
        SIM_2.m_end_time = SIM.m_end_time;

        SIM.use_simple_collisions = true;
        SIM_2.use_simple_collisions = true;
        SIM.init();
        SIM_2.init();

        SIM.run();
        SIM_2.run();

        std::sort (SIM.m_data.begin(), SIM.m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_gid < b->m_gid);});
        std::sort (SIM_2.m_data.begin(), SIM_2.m_data.end(), [](Particle * a, Particle * b) -> bool {return (a->m_gid < b->m_gid);});

        float acc = 0;
        float acc2 = 0;

        for(int i = 0; i < SIM.m_data.size(); ++i){
            acc += (SIM.m_data[i]->m_pos - SIM_2.m_data[i]->m_pos).len();
            acc2 += (SIM.m_data[i]->m_velocity - SIM_2.m_data[i]->m_velocity).len();
            // SIM.m_data[i]->m_pos.print();
            // SIM_simple.m_data[i]->m_pos.print();
            // printf("%lu %lu\n", SIM.m_data[i]->m_gid, SIM_simple.m_data[i]->m_gid);
        }

        test = acc < max_error;
        test = test && (acc2 < max_error);
        ret += !test;
        printif(test, "| Test 6: %.3e %.3e |", acc, acc2);
        omp_set_num_threads (omp_get_num_procs());
    }

    printf("\n");
    return ret;
}
REGISTER_MODULE(self_test);
